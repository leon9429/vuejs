# v-screen-handle="```options```"

## 设计目的
将电脑端鼠标（MouseEvent）和触屏端手势（TouchEvent）进行整合。

## 设计清单

| 事件名称 | 事件类型 |
| :--: | :--: |
| *基础事件* |
| 开始 | start |
| 移动 | moving |
| 结束 | end |
| 取消 | cancel |
| *点击事件* |
| 点击 | click \| tap |
| 双击 | doubleClick \| doubleTap |
| 狂击 | moreClick \| moreTap |
| 长按 | longClick \| longTap |
| *移动事件* |
| 滑动 | swipe |
| 快上 | swipeUp |
| 快下 | swipeDown |
| 快左 | swipeLeft |
| 快右 | swipeRight |
| *多点事件* |
| 捏放 | pinch |
| 拉开 | pinchOpen |
| 收紧 | pinchClose |
| 旋转 | rotate |
| 顺转 | rotateLeft \| clockwise |
| 逆转 | rotateRight \| anticlockwise |
