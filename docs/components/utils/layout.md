# layout.vue 开发文档

## 设计目的
- [x] 给主内容添加上下左右固定区域 (position: absolute)
- [x] 给主内容添加上下左右跟随区域 (position: relative)
- [x] 上下左右区域自动识别尺寸
- [x] 上下左右区域可设置尺寸
- [x] 左右区域可配置是否覆盖上下区域
```
|——————————————————————————————————————————|
| x |               TOP                | x |
|———|——————————————————————————————————|———|
|   | x |          B-TOP           | x |   |
|   |———|——————————————————————————|———|   |
|   |   |                          |   |   |
|   | B |                          | B |   |
| L | - |                          | - | R |
| E | L |        DEFAULT           | R | I |
| F | E |          BODY            | I | G |
| T | F |                          | G | H |
|   | T |                          | H | T |
|   |   |                          | T |   |
|   |   |                          |   |   |
|   |———|——————————————————————————|———|   |
|   | x |        B-BOTTOM          | x |   |
|———|——————————————————————————————————|———|
| x |             BOTTOM               | x |
|——————————————————————————————————————————|
```

----
## props
| 名称 | 功能 | 数据说明 | 默认数据 |
| :--: | :--: | :--: | :--: |
| width | 框架宽度 | [固定尺寸](#固定尺寸) | `'auto'` |
| height | 框架高度 | [固定尺寸](#固定尺寸) | `'auto'` |
| top | 固定上边设置 | [上下边](#上下边) | `'auto'` |
| bottom | 固定下边设置 | [上下边](#上下边) | `'auto'` |
| left | 固定左边设置 | [左右边](#左右边) | `'auto'` |
| right | 固定右边设置 | [左右边](#左右边) | `'auto'` |
| bodyTop | 内容上边设置（流式） | [上下边](#上下边) | `'auto'` |
| bodyBottom | 内容下边设置（流式） | [上下边](#上下边) | `'auto'` |
| bodyLeft | 内容左边设置（流式） | [左右边](#左右边) | `'auto'` |
| bodyRight | 内容右边设置（流式） | [左右边](#左右边) | `'auto'` |
| bodyMinwidth | 内容区最小宽度 | [固定尺寸](#固定尺寸) | `'auto'` |
| bodyMinheight | 内容区最小高度 | [固定尺寸](#固定尺寸) | `'auto'` |

----
## slots
### default
```html
<!-- 主内容区域 -->
<template slot="default"> ... </template>
<!-- or -->
<template> ... </template>
```
### top
```html
<!-- 固定上边区域 -->
<template slot="top"> ... </template>
```
### bottom
```html
<!-- 固定下边区域 -->
<template slot="bottom"> ... </template>
```
### left
```html
<!-- 固定左边区域 -->
<template slot="left"> ... </template>
```
### right
```html
<!-- 固定右边区域 -->
<template slot="right"> ... </template>
```
### body-top
```html
<!-- 内容上边区域 -->
<template slot="body-top"> ... </template>
```
### body-bottom
```html
    <!-- 内容下边区域 -->
    <template slot="body-bottom"> ... </template>
```
### body-left
```html
<!-- 内容左边区域 -->
<template slot="body-left"> ... </template>
```
### body-right
```html
<!-- 内容右边区域 -->
<template slot="body-right"> ... </template>
```

----
## 数据说明
### auto
**适用对象：** 所有
**说明：** 根据内容自动计算宽高值

### 固定尺寸
**类型：** [String, Number]
**适用：** width, bodyMinwidth, height, bodyMinheight
**说明：** 设置任意值 => 系统自动转换为 px 值


### 上下边
**类型：** [String, Number, Object]
**适用对象：** top, bottom, bodyTop, bodyBottom
**说明：**
- 直接设置 => '固定尺寸'
- 对象设置 => { height: [String, Number] }

### 左右边
**类型：** [String, Number, Object]
**适用对象：** left, right, bodyLeft, bodyRight
**说明：**
- 直接设置 => ```[String, Number] // 100px, 100%, 100 => 100```
- 对象设置
    ```
    {
        width: [String, Number], // 设置固定尺寸
        coverTop: [Boolean], // 设置是否遮盖同区域的上边
        coverBottom: [Boolean] // 设置是否遮盖同区域的下边
    }
    ```

## 样式注意事项
样式名 [class*="layout-"] 被区域化设置
覆盖 css 属性 'position, top, bottom, left, right, width, min-width, height, min-height, z-index, overflow, opacity'
