# i18n.vue 开发文档

## 设计目的
- [ ] 从 props 指定对象中用深度关键字读取值
- [ ] 从 window.i18n 中用深度关键字读取值
- [ ] 从远程请求中用深度关键字读取值

----
## props
| 名称 | 功能 | 数据说明 | 默认数据 |
| :--: | :--: | :--: | :--: |
| source | json 数据 | Object | `{}` |
| key * | 提取值关键字 | [深度关键字](#深度关键字) | `null` |
| default | 默认显示文字 | String | `''` |
| winkey | window 对象里的 key | [直取关键字](#直取关键字) | `'i18n'` |
| url | 异步请求地址 | String | `null` |

----
## 数据说明

### 直取关键字　
**类型:** `[String]`
**参考代码**
```javascript
var data = {
    key1: {
        key2: {
            key3: '1-2-3'
        }
    }
};
var key1 = 'key1.key2.key3'; // 深度关键字
var value = data[key1]; // undefined

var key2 = 'key1'; // 深度关键字
var value = data[key1]; // '1-2-3'
```

### 深度关键字
**类型:** `[String]`
**参考代码**
```javascript

import ObjectHelper from '@/helpers/object/deep';
var data = {
    key1: {
        key2: {
            key3: '1-2-3'
        }
    }
};
var key = 'key1.key2.key3'; // 深度关键字
var value = ObjectHelper.getValue(data, key); // '1-2-3'
```
