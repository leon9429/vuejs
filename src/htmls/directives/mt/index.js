import Vue from 'vue';
import MtHelper from '@/helpers/dom/mt';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    directives: {
        mt: {
            bind (el, binding, vnode) {
                let options = binding.value;
                // let modifiers = binding.modifiers;
                MtHelper(el, { ...options, debug: true });
            }
        }
    },
    components: {
        app: { template: '<div id="main"><slot></slot></div>' }
    },
    data: {
        items: []
    },
    methods: {
        click (s) {
            console.log(s);
        },
        dclick (s) {
            console.log(s);
        }
    },
    computed: {
    },
    mounted () {
        require('./index.less');
        console.log('[/directives/mts] => mounted.');
        for (let i = 1; i <= 30; i++) {
            this.items.push({ name: i });
        }
    }
});
