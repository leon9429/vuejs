import Vue from 'vue';
import FingerDirective from '@/directives/finger';
import SvgBaseComponent from '@/components/finger/svg-base';
import SvgMoveComponent from '@/components/finger/svg-move';
import SvgPinchComponent from '@/components/finger/svg-pinch';
import SvgRotateComponent from '@/components/finger/svg-rotate';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    directives: {
        finger: FingerDirective
    },
    components: {
        app: { template: '<div id="main"><slot></slot></div>' },
        svgBase: SvgBaseComponent,
        svgMove: SvgMoveComponent,
        svgPinch: SvgPinchComponent,
        svgRotate: SvgRotateComponent
    },
    data: {
        name: '...',
        points: [],
        scale: 1,
        rotate: 0,
        rect: {
            x: '50%',
            y: '50%',
            width: 150,
            height: 150,
            rotate: 0,
            scale: 1
        }
    },
    methods: {
        onStart (evt, info) {
            this.name = '...';
            this.points = [];
            this.points = JSON.parse(JSON.stringify(info.points));
        },
        onEnd (evt, info) {
            let newScale = this.rect.scale * this.scale;
            this.scale = 1;
            Vue.set(this.rect, 'scale', newScale);

            let newRotate = this.rect.rotate + this.rotate;
            this.rotate = 0;
            Vue.set(this.rect, 'rotate', newRotate);
        },
        onHandle (evt, info) {
            this.name = info.name;
        },
        onMoving (evt, info) {
            this.points = JSON.parse(JSON.stringify(info.points));
        },
        onSwipe (evt, info) {
            this.name = info.name + info.points[0].direction;
        },
        onPinch (evt, info) {
            let points = info.points;
            if (points[0].target !== points[1].target) return;
            this.scale = info.scale;
        },
        onRotate (evt, info) {
            let points = info.points;
            if (points[0].target !== points[1].target) return;
            this.rotate = info.rotate;
        }
    },
    computed: {
    },
    mounted () {
        require('./index.less');
        // console.log(ScreenHandle);
    }
});
