import Vue from 'vue';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    components: {
        app: { template: '<div id="main"><slot></slot></div>' }
    },
    data: {
    },
    methods: {
    },
    mounted () {
        require('./index.less');
        require(['vivus'], Vivus => {
            window.hi = new Vivus('hi-there', {
                type: 'scenario-sync',
                duration: 20,
                start: 'autostart',
                dashGap: 20,
                forceRender: false,
                onReady () {
                    this.parentEl.addEventListener('click', () => {
                        this.reset().play();
                    }, false);
                }
            }, () => {
                // animation is completed.
            });
            console.log('svg/vivusjs => mounted()');
        });
    }
});
