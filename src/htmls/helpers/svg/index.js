import Vue from 'vue';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    components: {
        app: { template: '<div id="main"><slot></slot></div>' }
    },
    data: {
    },
    methods: {
    },
    mounted () {
        require('./index.less');
        console.log('svg/pure => mounted()');
    }
});
