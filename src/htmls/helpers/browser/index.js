import Vue from 'vue';
import BrowserHelper from '@/helpers/browser';

/* eslint-disable no-new */
new Vue({
    el: '#app',
    components: {
        app: { template: '<div id="main"><slot></slot></div>' }
    },
    data: {
        info: {},
        cookies: [],
        cookie: {
            name: null,
            value: null
        }
    },
    methods: {
        setCookie () {
            if (!this.cookie.name) {
                return;
            }
            BrowserHelper.cookie.set(this.cookie.name, this.cookie.value);
            this.refreshCookies();
        },
        removeCookie (key) {
            BrowserHelper.cookie.remove(key);
            this.refreshCookies();
        },
        refreshCookies () {
            this.cookie.name = null;
            this.cookie.value = null;
            this.cookies = BrowserHelper.cookie.items;
        }
    },
    mounted () {
        this.info = {
            os: BrowserHelper.os,
            provider: BrowserHelper.provider
        };
        this.cookies = BrowserHelper.cookie.items;
    }
});
