import Vue from 'vue';
import LayoutComponent from '@/components/utils/layout.vue';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    components: {
        app: { template: '<div id="main"><slot></slot></div>' },
        LayoutComponent
    },
    data: {
        layoutWidth: 'auto',
        layoutHeight: 'auto',
        layoutTop: 'auto',
        layoutBottom: 'auto',
        layoutLeft: 'auto',
        layoutLeftCoverTop: false,
        layoutLeftCoverBottom: false,
        layoutRight: 'auto',
        layoutRightCoverTop: false,
        layoutRightCoverBottom: false,
        bodyHeight: 500,
        bodyMinwidth: 'auto',
        bodyMinheight: 'auto',
        bodyTop: 'auto',
        bodyBottom: 'auto',
        bodyLeft: 'auto',
        bodyLeftCoverTop: false,
        bodyLeftCoverBottom: false,
        bodyRight: 'auto',
        bodyRightCoverTop: false,
        bodyRightCoverBottom: false
    },
    methods: {
        changeLayoutWidth () {
            this.layoutWidth = this.layoutWidth === 'auto' ? 800 : 'auto';
        },
        changeLayoutHeight () {
            this.layoutHeight = this.layoutHeight === 'auto' ? 800 : 'auto';
        },
        changeLayoutTop () {
            this.layoutTop = this.layoutTop === 'auto' ? 80 : (this.layoutTop === 0 ? 'auto' : 0);
        },
        changeLayoutBottom () {
            this.layoutBottom = this.layoutBottom === 'auto' ? 80 : (this.layoutBottom === 0 ? 'auto' : 0);
        },
        changeLayoutLeft () {
            this.layoutLeft = this.layoutLeft === 'auto' ? 300 : (this.layoutLeft === 0 ? 'auto' : 0);
        },
        changeLayoutLeftCoverTop () {
            this.layoutLeftCoverTop = !this.layoutLeftCoverTop;
        },
        changeLayoutLeftCoverBottom () {
            this.layoutLeftCoverBottom = !this.layoutLeftCoverBottom;
        },
        changeLayoutRight () {
            this.layoutRight = this.layoutRight === 'auto' ? 300 : (this.layoutRight === 0 ? 'auto' : 0);
        },
        changeLayoutRightCoverTop () {
            this.layoutRightCoverTop = !this.layoutRightCoverTop;
        },
        changeLayoutRightCoverBottom () {
            this.layoutRightCoverBottom = !this.layoutRightCoverBottom;
        },
        changeBodyHeight () {
            this.bodyHeight = this.bodyHeight === 800 ? 1200 : this.bodyHeight === 500 ? 800 : 500;
        },
        changeBodyMinwidth () {
            this.bodyMinwidth = this.bodyMinwidth === 800 ? 1200 : this.bodyMinwidth === 'auto' ? 800 : 'auto';
        },
        changeBodyMinheight () {
            this.bodyMinheight = this.bodyMinheight === 'auto' ? '0' : 'auto';
        },
        changeBodyTop () {
            this.bodyTop = this.bodyTop === 'auto' ? 80 : (this.bodyTop === 0 ? 'auto' : 0);
        },
        changeBodyBottom () {
            this.bodyBottom = this.bodyBottom === 'auto' ? 80 : (this.bodyBottom === 0 ? 'auto' : 0);
        },
        changeBodyLeft () {
            this.bodyLeft = this.bodyLeft === 'auto' ? 300 : (this.bodyLeft === 0 ? 'auto' : 0);
        },
        changeBodyLeftCoverTop () {
            this.bodyLeftCoverTop = !this.bodyLeftCoverTop;
        },
        changeBodyLeftCoverBottom () {
            this.bodyLeftCoverBottom = !this.bodyLeftCoverBottom;
        },
        changeBodyRight () {
            this.bodyRight = this.bodyRight === 'auto' ? 300 : (this.bodyRight === 0 ? 'auto' : 0);
        },
        changeBodyRightCoverTop () {
            this.bodyRightCoverTop = !this.bodyRightCoverTop;
        },
        changeBodyRightCoverBottom () {
            this.bodyRightCoverBottom = !this.bodyRightCoverBottom;
        }
    },
    mounted () {
        require('./index.less');
        console.log('layout.js => mounted()');
    }
});
