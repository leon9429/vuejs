import Vue from 'vue';
import GridComponent from '@/components/utils/grid.vue';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    components: {
        app: { template: '<div id="main"><slot></slot></div>' },
        GridComponent
    },
    data: {
        colWidth: '',
        components: {
            grid: {
                columns: [12, 12]
            }
        }
    },
    methods: {
        addColumn () {
            this.components.grid.columns.push({ width: this.colWidth || 24 });
            this.colWidth = '';
        }
    },
    mounted () {
        require('./index.less');
    }
});
