import Vue from 'vue';
import MtDirective from '@/directives/mt';

require('font-awesome/css/font-awesome.min.css');

Vue.config.productionTip = false;

new Vue({
    el: '#app',
    directives: {
        mt: MtDirective
    },
    components: {
        app: { template: '<div id="main"><slot></slot></div>' }
    },
    data: {
        sourcedata: [],
        list: {
            info: {},
            x: 0,
            y: 0,
            moving: false,
            cancel: false,
            keeping: false,
            style: {},
            top: {
                style: {},
                editable: false,
                editing: false
            },
            item: {
                moving: false,
                editable: false,
                editing: false,
                cancelTimer: null
            },
            bottom: {
                editable: false,
                editing: false
            }
        },
        items: [],
        form: {}
    },
    computed: {
        hasMoving () {
            return this.list.moving !== false ||
                this.list.item.moving !== false;
        },
        hasEditing () {
            return this.list.top.editing !== false ||
                this.list.bottom.editing !== false ||
                this.list.item.editing !== false;
        },
        moveable () {
            return this.hasEditing === false &&
                this.hasMoving === false;
        }
    },
    watch: {
        sourcedata (newValue, oldValue) {
            this.items = [];
            setTimeout(() => {
                let result = [];
                newValue.filter(item => !item.deletedTime).forEach(item => {
                    result.push({
                        info: item,
                        style: {},
                        moving: false,
                        cancelable: false,
                        doneable: false,
                        deleteable: false
                    });
                });

                this.items = result.sort((p, n) => {
                    let prev = p.info;
                    let next = n.info;
                    if (prev.completedTime && next.completedTime) {
                        return next.completedTime - prev.completedTime;
                    } else if (prev.completedTime) {
                        return 1;
                    } else if (next.completedTime) {
                        return -1;
                    } else if (prev.order === next.order) {
                        if (prev.order > 0) {
                            return prev.createdTime - next.createdTime;
                        }
                    } else if (prev.order && next.order) {
                        return prev.order > next.order ? 1 : -1;
                    } else if (prev.order) {
                        return prev.order > 0 ? 1 : -1;
                    } else if (next.order) {
                        return next.order > 0 ? -1 : 1;
                    }
                    return next.createdTime - prev.createdTime;
                });
            });
        }
    },
    methods: {
        setForm (info) {
            this.form = {
                ...info
            };
            setTimeout(() => {
                document.querySelector('input').focus();
            });
        },
        resetForm () {
            if (this.list.top.editing) {
                this.list.y = 0;
            }
            if ((this.form.name || '').length) {
                if (this.list.item.editing) {
                    let item = this.sourcedata.find(item => item.id === this.form.id);
                    Object.assign(item, { ...this.form });
                    this.list.item.editing = false;
                } else {
                    this.form.id = Date.now();
                    if (this.list.top.editing) {
                        this.sourcedata.unshift({ ...this.form });
                        this.list.top.editing = false;
                    } else {
                        this.sourcedata.push({ ...this.form });
                        this.list.bottom.editing = false;
                    }
                }
                this.form = {};
            } else {
                this.list.cancel = true;
            }
        },
        addingOnEnd (info) {
            if (info.data.position === 'bottom' && (this.list.top.editing === true || this.list.bottom.editing === false)) return;
            if (this.list.item.editing === true) return;
            this.list.keeping = true;
        },
        listOnStart (info) {
            if (this.list.cancel === true) {
                this.list.cancel = false;
                this.list.top.editing = false;
                this.list.bottom.editing = false;
            }
        },
        listOnMoving (info) {
            if (this.list.moving === false &&
                this.moveable === false) {
                return;
            }

            this.list.moving = true;

            let translateY = this.list.y + info.points.last.first.offset.y;
            this.list.style = { transform: 'translateY(' + translateY + 'px)'};

            if (this.list.y === 0) {
                this.list.top.editable = translateY >= 60;

                let angle = Math.max(90 - (translateY / 60 * 100), 0);
                let opacity = 0.5 + (translateY / 60 * 0.5);
                this.list.top.style.transform = 'rotateX(' + angle + 'deg)';
                this.list.top.style.opacity = opacity;
            }
        },
        listOnEnd (info) {
            if (this.list.keeping) {
                this.list.keeping = false;
                return;
            }

            if (this.list.moving) {
                this.list.moving = false;
                this.list.y = this.list.y + info.points.last.first.offset.y;
                this.list.style = {};
                this.list.top.style = { transform: null, opacity: null };
                if (this.list.top.editable === false && this.list.y > 0) {
                    this.list.cancel = true;
                }
            } else if (this.hasEditing) {
                this.resetForm();
            } else {
                this.list.bottom.editable = this.list.item.editable === false;
            }
        },
        listOnSwipe (info) {
            if (this.list.top.editable === true) {
                this.list.top.editable = false;
                // adding at top
                this.list.y = 60;
                this.list.top.editing = true;
                this.setForm();
            } else if (this.moveable) {
                // scroll
                let v = info.points.last.first.offset.y / (info.points.last.first.t - info.points.first.first.t);
                let duration = v / ((v > 0 ? 1 : -1) * 0.006);
                let dist = v * (duration) / 2;
                this.list.y = Math.round(Math.min(0, Math.max(this.list.y + dist, info.el.parentElement.clientHeight - info.el.clientHeight))) || 0;
                this.list.style = { transform: 'translateY(' + this.list.y + 'px)', transition: Math.round(Math.max(duration, 300)) + 'ms' };
            }
        },
        listOnClick (info) {
            if (this.list.bottom.editable) {
                this.list.bottom.editable = false;
                this.list.bottom.editing = true;
                this.setForm();
            }
        },
        itemOnMoving (info) {
            let item = info.data;
            if (this.list.moving !== false ||
                this.hasEditing === true) {
                return;
            }
            clearTimeout(this.list.item.cancelTimer);
            this.list.item.moving = true;
            item.moving = true;

            let offsetX = info.points.last.first.offset.x;
            let $content = info.el.querySelector('.content');
            $content.style.transform = 'translateX(' + (offsetX > 0 ? Math.min(60, offsetX) : Math.max(offsetX, -60)) + 'px)';
            if (offsetX > 0) {
                let $icon = info.el.querySelector('.done');
                $icon.style.opacity = offsetX / 60;
            } else if (offsetX < 0) {
                let $icon = info.el.querySelector('.delete');
                $icon.style.opacity = Math.abs(offsetX) / 60;
            }
            let $line = info.el.querySelector('.line');
            $line.style.width = (offsetX / 60 * 100) + '%';

            item.cancelable = Math.abs(offsetX) < 60;
            item.doneable = offsetX >= 60;
            item.deleteable = offsetX <= -60;
            if (item.cancelable === false) {
                item.style = { transform: 'translateX(' + (offsetX - (offsetX > 0 ? 60 : -60)) + 'px)' };
            }
        },
        itemOnEnd (info) {
            let item = info.data;

            if (item.moving === false) {
                this.list.item.editable = this.list.item.editing.id === item.info.id ||
                    (this.hasMoving === false && this.hasEditing === false);
            }
        },
        itemOnSwipe (info) {
            this.itemOnCancel(info);
        },
        itemOnCancel (info) {
            let item = info.data;
            let transitionTime = 0;
            if (item.cancelable && item.moving) {
                let v = info.points.last.first.offset.x / (info.points.last.first.t - info.points.first.first.t);
                let duration = v / ((v > 0 ? 1 : -1) * 0.006) || 0;
                transitionTime = Math.round(Math.max(duration, 300));
                item.style = {
                    transition: 'all ' + transitionTime + 'ms',
                    transform: 'translateX(' + 0 + 'px)'
                };

                let $content = info.el.querySelector('.content');
                $content.style.transform = 'translateX(0px)';
                $content.style.transition = 'all ' + transitionTime + 'ms';

                let $line = info.el.querySelector('.line');
                $line.style.width = null;

                setTimeout(() => {
                    $content.style = null;
                }, transitionTime);
            } else {
                transitionTime = 300;
                if (item.doneable) {
                    this.itemDoDone(item.info.id);
                } else if (item.deleteable) {
                    this.itemDoDelete(item.info.id);
                }
            }
            item.moving = false;
            setTimeout(() => {
                item.style = {};
                item.cancelable = false;
            }, transitionTime);
            this.list.item.cancelTimer = setTimeout(() => {
                this.list.item.moving = false;
            }, transitionTime);
        },
        itemOnClick (info) {
            if (this.list.item.editable === true) {
                this.list.item.editable = false;

                let item = info.data;
                this.list.item.editing = item.info;
                this.setForm(item.info);
            }
        },
        itemDoDone (id) {
            let index = this.sourcedata.findIndex(item => item.id === id);
            let item = this.sourcedata[index];
            item.completedTime = Date.now();
            Vue.set(this.sourcedata, index, item);
        },
        itemDoDelete (id) {
            let index = this.sourcedata.findIndex(item => item.id === id);
            let item = this.sourcedata[index];
            item.deletedTime = Date.now();
            Vue.set(this.sourcedata, index, item);
        }
    },
    mounted () {
        require('./index.less');
        for (let i = 0; i < 5; i++) {
            this.sourcedata.push({ id: 'item-' + i, name: 'name-' + i, createdTime: null, completedTime: null, deletedTime: null });
        }
    }
});
