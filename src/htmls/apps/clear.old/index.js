import Vue from 'vue';
import FingerDirective from '@/directives/finger';

require('font-awesome/css/font-awesome.min.css');

Vue.config.productionTip = false;

new Vue({
    el: '#app',
    directives: {
        finger: FingerDirective
    },
    components: {
        app: { template: '<div id="main"><slot></slot></div>' }
    },
    data: {
        items: [],
        parentId: 0, // 通过这个 parentId 提取列表项
        form: {
            id: null,
            parentId: 0, // 数据保存时的 parentId, setForm() 会自动设置
            name: ''
        },
        list: {
            top: 0,
            moving: false,
            topready: false, // 控制顶部文字提示
            topadding: false,
            bottomadding: false,
            bottomaddable: true,
            cancelmode: '',
            styles: {
                transform: 'translateY(0px)',
                transition: null
            },
            topStyles: {
                transform: 'rotateX(90deg)',
                opacity: '0'
            }
        },
        item: {
            started: false,
            moving: false,
            editing: false
        }
    },
    computed: {
        groupItems () {
            let result = [];
            result = this.items.filter(item => item.parentId === this.parentId);
            return result;
        },
        normalItems () {
            let result = [];
            result = this.groupItems.filter(item => item.done !== true);
            return result;
        },
        doneItems () {
            let result = [];
            result = this.groupItems.filter(item => item.done === true);
            return result;
        },
        listClasses () {
            return [{
                editing: this.isEditing,
                topready: this.list.topready,
                topadding: this.list.topadding,
                bottomadding: this.list.bottomadding,
                moving: this.list.moving
            }, this.list.cancelmode];
        },
        isEditing () {
            return this.list.topadding || this.list.bottomadding || this.item.editing;
        }
    },
    watch: {
        'list.cancelmode' (newValue) {
            if (newValue) {
                this.list.topadding = false;
                this.list.bottomadding = false;
                this.list.top = 0;
                this.list.styles.transform = 'translateY(0px)';
                this.list.topStyles.transform = 'rotateX(90deg)';
                this.list.topStyles.opacity = '0.5';
            }
        }
    },
    methods: {
        sectionStart ($event, info) {
            console.log('section.start');
            this.list.cancelmode = '';
            this.list.styles.transition = null;
            this.list.bottomaddable = false;
        },
        sectionMoving ($event, info) {
            console.log('section.moving');
            if (this.isEditing || this.item.moving) {
                return;
            }
            console.log('section.moving =>');
            let topable = this.list.top === 0;
            let offsetY = this.list.top + info.points[0].to.y - info.points[0].from.y;
            this.list.styles.transform = 'translateY(' + Math.round(offsetY) + 'px)';
            if (offsetY > 15 && topable) {
                let angle = 90 - (offsetY / 60 * 100);
                let opacity = 0.5 + (offsetY / 60 * 0.5);
                this.list.topStyles.transform = 'rotateX(' + Math.max(angle, 0) + 'deg)';
                this.list.topStyles.opacity = opacity;
                this.list.topready = Math.abs(offsetY) >= 60;
            }
            this.list.topadding = false;
            this.list.moving = true;
        },
        sectionEnd ($event, info) {
            console.log('section.end');
            if (this.item.moving) {
                this.item.moving = false;
                return;
            }
            console.log('section.end =>');
            if (!this.item.started) {
                // 直接点击的处理
                if (this.isEditing) {
                    this.save();
                } else {
                    this.list.bottomaddable = true;
                }
            }

            if (this.list.moving) {
                let el = info.el.querySelector('ul');
                let maxHeight = info.el.clientHeight - el.clientHeight;
                let topable = this.list.top === 0;
                this.list.top += info.points[0].to.y - info.points[0].from.y;
                // if (this.list.top >= 180) {
                //     // 下拉过度
                // } else if (this.list.top >= 60) {
                if (topable && this.list.top > 0) {
                    if (this.list.top >= 60) {
                        // 顶部添加
                        this.list.styles.transform = 'translateY(60px)';
                        this.list.topStyles.transform = 'rotateX(0deg)';
                        this.list.topStyles.opacity = '1';
                        this.list.topadding = true;
                        this.list.top = 59;
                        this.setForm();
                    } else if (this.list.top > 0) {
                        // 还原
                        if (!this.list.topadding) {
                            this.list.cancelmode = 'ready-to-cancel';
                        }
                    }
                } else {
                    // 保持
                    let offset = info.points[0].to.offset;
                    let v = offset.y / offset.t;
                    let duration = v / ((v > 0 ? 1 : -1) * 0.006);
                    let dist = v * (duration) / 2;
                    this.list.top = Math.round(Math.min(0, Math.max(this.list.top + dist, maxHeight)));
                    this.list.styles.transform = 'translateY(' + this.list.top + 'px)';
                    this.list.styles.transition = 'all ' + Math.round(Math.max(duration, 300)) + 'ms ease';
                }
            } else if (this.list.bottomaddable) {
                this.list.bottomadding = true;
                this.setForm();
            }
            this.list.topready = false;
            this.list.moving = false;
            this.item.started = false;
        },
        sectionItemStart ($event, info) {
            $event.stopPropagation();
            if (info.data === 'bottom') {
                this.list.bottomadding = true;
                this.setForm();
            }
        },
        itemStart ($event, info) {
            console.log('item.start');
            this.item.started = true;
        },
        itemMoving ($event, info) {
            console.log('item.moving');
            if (this.list.moving || this.isEditing) {
                return;
            }
            if (info.points.length === 1 && ['left', 'right'].indexOf(info.points[0].direction.toLowerCase()) !== -1) {
                $event.stopPropagation();
                console.log('item.moving =>');
                if (info.data.done !== true) {
                    info.el.style.transition = null;
                    info.el.style.transform = 'translateX(' + info.points[0].to.offset.x + 'px)';
                }
                this.item.moving = true;
            }
        },
        itemCancel ($event, info) {
            console.log('item.cancel');
            if (this.list.moving) {
                return;
            }
            console.log('item.cancel =>');
            info.el.style.transition = 'transform 0.3s ease';
            info.el.style.transform = 'translateX(0px)';
            setTimeout(() => {
                info.el.style = null;
                this.itemRefresh();
            }, 250);
        },
        itemEnd ($event, info) {
            console.log('item.end');
            if (this.list.moving) {
                return;
            }
            console.log('item.end =>');

            if (this.item.moving) {
                if (info.points[0].to.offset.x <= -80) {
                    info.el.style.transform = 'translateX(-100%)';
                    info.el.style.transition = 'transform .3s ease, height .3s ease .3s';
                    this.itemRemoved({ id: info.el.id });
                    return;
                } else if (info.points[0].to.offset.x >= 80) {
                    this.itemDone({ id: info.el.id });
                }
                this.itemCancel($event, info);
            } else if (!this.isEditing && info.data.done !== true) {
                this.setForm(info.data);
            } else if (info.data.id !== this.form.id) {
                this.save();
            }
        },
        itemDone (itemInfo) {
            if (itemInfo.id) {
                let item = this.items.find(item => item.id === itemInfo.id);
                item.done = true;
                item.doneTime = Date.now();
            }
        },
        itemRemoved (itemInfo) {
            if (itemInfo.id) {
                this.items.find(item => item.id === itemInfo.id).removed = true;
                setTimeout(() => {
                    this.items.splice(this.items.findIndex(item => item.id === itemInfo.id), 1);
                });
            }
        },
        itemRefresh () {
            this.items.sort((prev, next) => {
                if (prev.done === true && next.done === true) {
                    return next.doneTime - prev.doneTime;
                } else if (prev.done === true) {
                    return 1;
                } else if (next.done === true) {
                    return -1;
                } else if (prev.order === next.order) {
                    if (prev.order > 0) {
                        return prev.createdTime - next.createdTime;
                    }
                } else if (prev.order && next.order) {
                    return prev.order > next.order ? 1 : -1;
                } else if (prev.order) {
                    return prev.order > 0 ? 1 : -1;
                } else if (next.order) {
                    return next.order > 0 ? -1 : 1;
                }
                return next.createdTime - prev.createdTime;
            });
        },
        setForm (info) {
            Object.assign(this.form, info);
            this.form.parentId = this.parentId;
            if (this.form.id) {
                this.item.editing = true;
            }
            // 这里统一设置输入信息
            setTimeout(() => {
                let $input = this.$refs.input;
                if (this.form.id !== null) {
                    $input = this.$refs.input[0];
                }
                $input.focus();
            }, 100);
        },
        cancel () {
            if (this.list.topadding) {
                this.reset('top-to-cancel');
            } else if (this.list.bottomadding) {
                this.reset('bottom-to-cancel');
            } else {
                this.reset('item-to-cancel');
            }
        },
        save () {
            let newInfo = {
                id: 'i' + new Date().getTime('x'),
                done: false,
                removed: false,
                createdTime: Date.now(),
                doneTime: null
            };
            let info = this.items.find(item => { return item.id === this.form.id; }) || newInfo;
            let name = this.form.name.replace(/^\s|\s$/ig, '');
            Object.assign(info, this.form, { id: info.id, name });
            if (this.list.topadding) {
                info.order = -1;
                this.items.unshift(info);
                this.list.top = 0;
                this.list.styles.transform = null;
            } else if (this.list.bottomadding) {
                info.order = 1;
                this.items.push(info);
            }
            setTimeout(() => {
                if (name.length === 0) {
                    this.items.splice(this.items.findIndex(item => { return item.id === info.id; }), 1);
                }
            });
            this.reset();
        },
        reset (cancelmode) {
            this.list.topadding = false;
            this.list.bottomadding = false;
            this.item.editing = false;
            this.list.cancelmode = cancelmode;
            this.form.id = null;
            this.form.name = '';
        }
    },
    mounted () {
        require('./index.less');
        console.log('/apps/clear.html => mounted()');
        for (let i = 0; i < 15; i++) {
            this.items.push({
                name: 'item-' + i,
                parentId: 0,
                id: 'i' + i,
                done: i % 5 === 0,
                removed: false,
                createdTime: i,
                doneTime: i % 5 === 0 ? i + 1 : null
            });
        }
        this.itemRefresh();
    }
});
