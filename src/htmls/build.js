import Vue from 'vue';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    components: {
        app: { template: '<div id="main"><slot></slot></div>' }
    },
    mounted () {
        console.log('html.js => mounted()');
    }
});
