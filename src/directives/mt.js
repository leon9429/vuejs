import MtHelper from '@/helpers/dom/mt';

export default {
    inserted (el, binding, vnode) {
        let options = { ...binding.value, debug: true };
        new MtHelper(el, options);
    }
};
