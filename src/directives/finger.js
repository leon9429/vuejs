import FingerHelper from '@/helpers/dom/finger';

export default {
    inserted (el, binding, vnode) {
        let options = Object.assign({}, binding.value);
        new FingerHelper(el, options);
    }
};
