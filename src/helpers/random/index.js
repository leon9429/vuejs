import ColorRandom from './color';
import NumberRandom from './number';

export default {
    get number () {
        return NumberRandom;
    },
    get color () {
        return ColorRandom;
    }
};
