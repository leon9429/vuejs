export default {
    /**
     * 返回一个在指定范围内的随机数
     * @param  {Number} min
     * @param  {Number} max
     * @return {Number}
     */
    range (min, max) {
        return Math.round(min + Math.random() * (max - min));
    },
    /**
     * 返回一个在 0 ~ 指定返回内的随机数
     * @param  {Number} value
     * @return {Number}
     */
    max (value) {
        return this.range(0, value);
    }
};
