import NumberRandom from './number';

export default {
    get hex () {
        return '#' + ('00000' + (Math.random() * 0x1000000 << 0).toString(16)).slice(-6);
    },
    get r () {
        return NumberRandom.max(255);
    },
    get g () {
        return NumberRandom.max(255);
    },
    get b () {
        return NumberRandom.max(255);
    },
    get rgb () {
        return 'rgb(' + this.r + ', ' + this.g + ', ' + this.b + ')';
    },
    get rgba () {
        return 'rgba(' + this.r + ', ' + this.g + ', ' + this.b + ', ' + Math.random().toFixed(2) + ')';
    }
};
