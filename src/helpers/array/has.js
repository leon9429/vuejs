/* eslint-disable no-extend-native */
if (!Array.prototype.hasValue) {
    Object.defineProperty(Array.prototype, 'hasValue', {
        value: function (value) {
            return this.indexOf(value) !== -1;
        }
    });
} else {
    console.warn('Array.prototype.hasValue is exist in native.');
}

if (!Array.prototype.hasnotValue) {
    Object.defineProperty(Array.prototype, 'hasnotValue', {
        value: function (value) {
            return this.indexOf(value) === -1;
        }
    });
} else {
    console.warn('Array.prototype.hasnotValue is exist in native.');
}

export default {
    /**
     * 返回数组是否还有指定值
     *
     * @param  { Array } array
     * @param  { * } value
     * @return { Boolean }
     */
    hasValue (array, value) {
        if (array instanceof Array) {
            return array.hasValue(value);
        }
        throw new Error('array is not an Array.');
    },
    /**
     * 返回数组是否没有指定值
     *
     * @param  { Array } array
     * @param  { * } value
     * @return { Boolean }
     */
    hasnotValue (array, value) {
        if (array instanceof Array) {
            return array.hasnotValue(value);
        }
        throw new Error('array is not an Array.');
    }
};
