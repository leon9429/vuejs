/* eslint-disable no-extend-native */
if (!Array.prototype.valuesFrom) {
    Object.defineProperty(Array.prototype, 'valuesFrom', {
        value: function (from = 0, many) {
            let result = [...this].splice(from, Math.abs(many));
            if (many <= 0) {
                result = [...this].reverse().splice((from + 1) * -1, Math.abs(many)).reverse();
            }
            return result;
        }
    });
} else {
    console.warn('Array.prototype.valuesFrom is exist in native.');
}

if (!Array.prototype.valueAt) {
    Object.defineProperty(Array.prototype, 'valueAt', {
        value: function (index) {
            if (index <= 0) {
                return [...this].splice(index, 1)[0];
            }
            return this[index];
        }
    });
} else {
    console.warn('Array.prototype.valueAt is exist in native.');
}

export default {
    /**
     * 获取数组中指定位置的值
     *
     * @param  { Array } array
     * @param  { Number } index
     * @return { * }
     */
    valueAt (array, index) {
        if (array instanceof Array) {
            return array.valueAt(index);
        }
        throw new Error('array is not an Array.');
    },
    /**
     * 获取数组中指定位置(from)开始的几个(many)元素
     * - from 的正负值决定结果提取是从头还是从尾标记
     * - many 的正负值决定结果提取是往前还是往后数数
     *
     * @param  { Array } array
     * @param { Number } from
     * @param { Number } many
     * @return { Array(*) }
     * @example
     *  [1, 2, 3, 4, 5, 6, 7, 8, 9].valuesFrom(2, 3); // => [3, 4, 5]
     *  [1, 2, 3, 4, 5, 6, 7, 8, 9].valuesFrom(-2, 2); // => [8, 9]
     *  [1, 2, 3, 4, 5, 6, 7, 8, 9].valuesFrom(-3, -2); // => [6, 7]
     *  [1, 2, 3, 4, 5, 6, 7, 8, 9].valuesFrom(0, 3); // => [1，2, 3]
     *  [1, 2, 3, 4, 5, 6, 7, 8, 9].valuesFrom(0, -3); // => [1]
     */
    valuesFrom (array, from, many) {
        if (array instanceof Array) {
            return array.valuesFrom(from, many);
        }
        throw new Error('array is not an Array.');
    }
};
