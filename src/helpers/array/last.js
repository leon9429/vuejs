/* eslint-disable no-extend-native */
if (!Array.prototype.last) {
    Object.defineProperty(Array.prototype, 'last', {
        get () {
            return this[this.length - 1];
        },
        set (value) {
            this.push(value);
        }
    });
} else {
    console.warn('Array.prototype.last is exist in native.');
}

export default {
    /**
     * 获取数组的最后一个元素
     *
     * @param  { Array } array
     * @return { * }
     */
    last (array) {
        if (array instanceof Array) {
            return array.last;
        }
        throw new Error('array is not an Array.');
    }
};
