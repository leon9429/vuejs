import First from './first';
import Last from './last';
import Value from './value';
import Has from './has';
import Remove from './remove';

export default {
    ...First,
    ...Last,
    ...Value,
    ...Has,
    ...Remove
};
