/* eslint-disable no-extend-native */
if (!Array.prototype.removeValue) {
    Object.defineProperty(Array.prototype, 'removeValue', {
        value: function (value) {
            let result = this.splice(this.indexOf(value), 1);
            return result;
        }
    });
} else {
    console.warn('Array.prototype.removeValue is exist in native.');
}

export default {
    /**
     * 返回数组是否还有指定值
     *
     * @param  { Array } array
     * @param  { * } value
     * @return { Boolean }
     */
    removeValue (array, value) {
        if (array instanceof Array) {
            return array.removeValue(value);
        }
        throw new Error('array is not an Array.');
    }
};
