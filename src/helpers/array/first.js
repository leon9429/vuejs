/* eslint-disable no-extend-native */
if (!Array.prototype.first) {
    Object.defineProperty(Array.prototype, 'first', {
        get () {
            return this[0];
        },
        set (value) {
            this.unshift(value);
        }
    });
} else {
    console.warn('Array.prototype.first is exist in native.');
}

export default {
    /**
     * 获取数组的第一个值
     *
     * @param  { Array } array
     * @return { * }
     */
    first (array) {
        if (array instanceof Array) {
            return array.first;
        }
        throw new Error('array is not an Array.');
    }
};
