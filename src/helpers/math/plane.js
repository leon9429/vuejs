/**
 * 数学平面坐标系
 */
const DIRECTIONS = {
    NONE: 'none',
    UP: 'up',
    DOWN: 'down',
    LEFT: 'left',
    RIGHT: 'right'
};

export default {
    get const () {
        return { directions: { ...DIRECTIONS } };
    },
    /**
     * @summary 获取一个点所在坐标系的象限位置
     *
     * @param  { Object } point
     *  - @property { Number } x
     *  - @property { Number } y
     *
     * @return { Number | null } 0 ~ 4 | null
     *  • 0: 原点
     *  • 1~4: 正常象限
     *  • null: 不在任何象限，x 或 y 为 0
     */
    getPointQuadrant (point) {
        let x = point.x;
        let y = point.y;
        if (x * y === 0) {
            if (x === y) return 0;
            return null;
        } else if (x > 0) {
            if (y > 0) return 1;
            return 4;
        } else {
            if (y > 0) return 2;
            return 3;
        }
    },
    /**
     * @summary 获取两个坐标点的直线距离
     *
     * @description
     *  思考步骤：
     *    1. 直角三角形：勾三股四弦五
     *    2. 直角边 x = p2.x - p1.x // => 3
     *    3. 直角边 y = p2.y - p1.y // => 4
     *    4. 斜边 = 平方根(平方(x) + 平方(y)) // => 5
     *
     * @param { Object } p1
     *  - @property { Number } x
     *  - @property { Number } y
     *
     * @param { Object } p2 目标点，默认为 { x: 0, y: 0 }
     *  - @property { Number } x
     *  - @property { Number } y
     *
     * @return { Number } 返回距离数值
     */
    getTwoPointsDistance (p1 = { x: 0, y: 0 }, p2 = { x: 0, y: 0 }) {
        return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
    },
    /**
     * @summary 获取两个坐标点的偏差数据
     *
     * @param { Object } p1
     *  - @property { Number } x
     *  - @property { Number } y
     *
     * @param { Object } p2 目标点，默认为 { x: 0, y: 0 }
     *  - @property { Number } x
     *  - @property { Number } y
     *
     * @return { Object } 返回距离数值
     *  - @property { Number } x
     *  - @property { Number } y
     */
    getTwoPointsOffset (p1 = { x: 0, y: 0 }, p2 = { x: 0, y: 0}) {
        return {
            x: p2.x - p1.x,
            y: p2.y - p1.y
        };
    },
    /**
     * @summary 获取「p2」相对于「p1」的大方向
     *
     * @param { Object } p1
     *  - @property { Number } x
     *  - @property { Number } y
     *
     * @param { Object } p2 方向点（p1 -> p2），默认为 { x: 0, y: 0 }
     *  - @property { Number } x
     *  - @property { Number } y
     *
     * @return { Object }
     *  - @property { Number } x
     *  - @property { Number } y
     */
    getTwoPointsDirection (p1, p2) {
        let offset = this.getTwoPointsOffset(p1, p2);
        if (Math.abs(offset.x) === Math.abs(offset.y)) {
            return DIRECTIONS.NONE;
        }
        return Math.abs(offset.x) > Math.abs(offset.y) ? (offset.x < 0 ? DIRECTIONS.LEFT : DIRECTIONS.RIGHT) : (offset.y < 0 ? DIRECTIONS.UP : DIRECTIONS.DOWN);
    },
    /**
     * @summary 获取「p2」相对于「p1」的方位信息（东e南s西w北n）
     *
     * @param { Object } p1
     *  - @property { Number } x
     *  - @property { Number } y
     *
     * @param { Object } p2 方向点（p1 -> p2），默认为 { x: 0, y: 0 }
     *  - @property { Number } x
     *  - @property { Number } y
     *
     * @return { Object }
     *  - @property { Boolean } n
     *  - @property { Boolean } s
     *  - @property { Boolean } w
     *  - @property { Boolean } e
     */
    getTwoPointsPosition (p1, p2) {
        let offset = this.getTwoPointsOffset(p1, p2);
        return {
            n: offset.y < 0,
            s: offset.y > 0,
            w: offset.x < 0,
            e: offset.x > 0
        };
    },
    /**
     * @summary 获取两个坐标点的夹角
     *
     * @description
     *  思考步骤：
     *    1. 把 p1 看成原点
     *    2. 画斜边 p1->p2 => x = p2.x - p1.x
     *    3. 直角边 p2.y -> p1.x => y = p2.y - p1.y
     *    4. 弧度 = Math.atan(y, x)
     *    5. 角度 = 180 / π × 弧度
     *  角度值说明：
     *    • 正角度为顺时针方向的夹角，负角度为逆时针方向的夹角
     *
     * @param { Object } p1
     *  - @property { Number } x
     *  - @property { Number } y
     *
     * @param { Object } p2 方向点（p1 -> p2），默认为 { x: 0, y: 0 }
     *  - @property { Number } x
     *  - @property { Number } y
     *
     * @return { Number } 角度值 -179 ~ 180
     */
    getTwoPointsAngle (p1, p2 = { x: 0, y: 0 }) {
        return Math.atan2(p2.y - p1.y, p2.x - p1.x) * 180 / Math.PI;
    },
    /**
     * @summary 获取两个向量的夹角
     *
     * @description
     *  参考两向量夹角公式
     *  给出了坐标
     *  先求出两个向量的模
     *  再求出两个向量的向量积
     *  |a|=√[x1^2+y1^2]
     *  |b|=√[x2^2+y2^2]
     *  a*b=(x1,y1)(x2,y2)=x1x2+y1y2
     *  cos=a*b/[|a|*|b|]=(x1x2+y1y2)/[√[x1^2+y1^2]*√[x2^2+y2^2]]
     *
     * @see http://old.pep.com.cn/gzsx/jszx_1/jxyj/gzsxjscg/201104/t20110421_1038297.htm
     * @see https://baike.baidu.com/item/%E5%A4%B9%E8%A7%92%E5%85%AC%E5%BC%8F/6773533?fr=aladdin
     * @see https://www.zybang.com/question/bf5f678e826a73f3ff743c1622b4b498.html
     *
     * @param { Object } v1 两点结束的向量差 => p2.to - p1.to
     *  - @property { Number } x p2.to.x - p1.to.x
     *  - @property { Number } y p2.to.y - p1.to.y
     *
     * @param { Object } v2 两点开始的向量差 => p2.from - p1.from
     *  - @property { Number } x p2.from.x - p1.from.x
     *  - @property { Number } y p2.from.y - p1.from.y
     *
     * @return { Number } 角度值 -179 ~ 180
     */
    getTwoVectorsAngle (v1, v2) {
        let d1 = this.getTwoPointsDistance(v1);
        let d2 = this.getTwoPointsDistance(v2);
        let cos = (v1.x * v2.x + v1.y * v2.y) / (d1 * d2);
        // if (cos > 1) cos = 1;
        let angle = Math.acos(cos);
        if ((v1.x * v2.y - v1.y * v2.x) > 0) {
            angle *= -1;
        }
        return angle * 180 / Math.PI;
    },
    /**
     * @summary 获取两个向量的比例
     *
     * @param { Object } v1 两点结束的向量差 => p2.to - p1.to
     *  - @property { Number } x p2.to.x - p1.to.x
     *  - @property { Number } y p2.to.y - p1.to.y
     *
     * @param { Object } v2 两点开始的向量差 => p2.from - p1.from
     *  - @property { Number } x p2.from.x - p1.from.x
     *  - @property { Number } y p2.from.y - p1.from.y
     *
     * @return { Number } 比例值 >= 0
     */
    getTwoVectorsScale (v1, v2) {
        return this.getTwoPointsDistance(v1) / this.getTwoPointsDistance(v2);
    }
};
