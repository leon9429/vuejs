/* eslint-disable no-extend-native */
if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
    };
}

export default {
    trim (value) {
        if (typeof value === 'string') {
            return value.trim();
        }
        throw new Error('value is not string.');
    }
};
