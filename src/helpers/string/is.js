import './trim';

/* eslint-disable no-extend-native */
if (!String.prototype.isEmpty) {
    String.prototype.isEmpty = function () {
        return this.trim().length === 0;
    };
}

export default {
    isEmpty (value) {
        if (typeof value === 'string') {
            return value.isEmpty();
        }
        throw new Error('value is not string.');
    }
};
