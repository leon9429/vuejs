import Trim from './trim';
import Is from './is';
import In from './in';
import Start from './start';

export default {
    ...Trim,
    ...Is,
    ...In,
    ...Start
};
