/* eslint-disable no-extend-native */
if (!String.prototype.startWith) {
    Object.defineProperty(String.prototype, 'startWith', {
        value: function (string) {
            if (typeof string !== 'string') {
                throw new Error('string is not String.');
            }
            return this.substr(0, string.length) === string;
        }
    });
} else {
    console.warn('String.prototype.startWith is exist in native.');
}

export default {
    startWith (string, target) {
        if (typeof target !== 'string') {
            throw new Error('target is not String.');
        }
        return value.inArray(array);
    }
};
