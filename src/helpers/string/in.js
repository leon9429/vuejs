/* eslint-disable no-extend-native */
if (!String.prototype.inArray) {
    Object.defineProperty(String.prototype, 'inArray', {
        value: function (array) {
            if (!(array instanceof Array)) {
                throw new Error('array is not Array.');
            }
            return array.indexOf(this) !== -1;
        }
    });
} else {
    console.warn('String.prototype.inArray is exist in native.');
}

if (!String.prototype.notinArray) {
    Object.defineProperty(String.prototype, 'notinArray', {
        value: function (array) {
            if (!(array instanceof Array)) {
                throw new Error('array is not Array.');
            }
            return array.indexOf(this) === -1;
        }
    });
} else {
    console.warn('String.prototype.notinArray is exist in native.');
}

export default {
    inArray (value, array) {
        if (typeof value !== 'string') {
            throw new Error('value is not String.');
        }
        return value.inArray(array);
    },
    notinArray (value, array) {
        if (typeof value !== 'string') {
            throw new Error('value is not String.');
        }
        return value.notinArray(array);
    }
};
