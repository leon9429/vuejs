const helper = {
    /**
     * 只读
     * @return {RegExp} 返回 url 的正则表达式
     */
    get pattern () {
        // return /^(https?|ftp|file):\/\/.*/i;
        return new RegExp('^(https?|ftp|file)://.*', 'i');
    },
    /**
     * 检测是否匹配
     * @param  {String}  value
     * @return {Boolean}
     */
    isMatch (value) {
        return this.pattern.test(value);
    }
};

export default helper;
