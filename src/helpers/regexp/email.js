const helper = {
    /**
     * 只读
     * @return {RegExp} 返回 email 的正则表达式
     */
    get pattern () {
        return /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
    },
    /**
     * 检测是否匹配
     * @param  {String}  value
     * @return {Boolean}
     */
    isMatch (value) {
        return this.pattern.test(value);
    }
};

export default helper;
