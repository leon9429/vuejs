import EmailRegexp from './email';
import UrlRegexp from './url';
import ColorRegexp from './color';

const helpers = {
    /**
     * 检测字符串是否是 email 格式
     * @param  {String}  value
     * @return {Boolean}
     */
    isEmail (value) {
        return EmailRegexp.isMatch(value);
    },
    /**
     * 检测字符串是否是 url 格式
     * @param  {String}  value
     * @return {Boolean}
     */
    isUrl (value) {
        return UrlRegexp.isMatch(value);
    },
    /**
     * 检测字符串是否是 color 格式
     * @param  {String}  value
     * @return {Boolean}
     */
    isColor (value) {
        return ColorRegexp.isMatch(value);
    }
};

export default helpers;
