const helper = {
    /**
     * 只读
     * @return {RegExp} 返回 color 的正则表达式
     */
    get pattern () {
        return /^#([0-9a-fA-F]{6}|[0-9a-fA-F]{3})$|^rgb[a]?[(]([\s]*(2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?),){2}[\s]*(2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?),?[\s]*(0\.\d{1,2}|1|0)?[)]{1}$/ig;
    },
    // get patterns () {
    //     return {
    //         hex: /^#([0-9a-fA-F]{6}|[0-9a-fA-F]{3})$/g,
    //         rgba: /^rgb[a]?[(]([\s]*(2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?),){2}[\s]*(2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?),?[\s]*(0\.\d{1,2}|1|0)?[)]{1}$/ig
    //     };
    // },
    /**
     * 检测是否匹配
     * @param  {String}  value
     * @return {Boolean}
     */
    isMatch (value) {
        return this.pattern.test(value);
    }
};

export default helper;
