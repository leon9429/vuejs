export default {
    isString (value) {
        return typeof value === 'string';
    },
    isNumber (value) {
        if (this.isString(value)) {
            return /^\d+(\.\d+)?$/.test(value);
        }
        return typeof value === 'number';
    },
    isArray (value) {
        return value instanceof Array;
    },
    isObject (value) {
        return !this.isArray(value) && typeof value === 'object';
    },
    isBoolean (value) {
        return typeof value === 'boolean';
    },
    isNull (value) {
        return value === null;
    },
    isUndefined (value) {
        return typeof value === 'undefined';
    },
    isEmpty (value) {
        return [
            JSON.stringify(''),
            '{}',
            '[]',
            'null',
            undefined
        ].indexOf(JSON.stringify(value)) !== -1;
    }
};
