import Type from './type';
import Value from './value';

let helper = function (value) {
    this.value = value;
};

helper.prototype = {
    valueOf () {
        return this.value;
    },
    toString () {
        if (this.isObject()) {
            return JSON.stringify(this.value);
        }
        return this.value.toString();
    }
};

Object.keys(Type).forEach(key => {
    // if (typeof Type[key] === 'function') {
    helper.prototype[key] = function () {
        return Type[key](this.valueOf());
    };
    // }
});

Object.keys(Value).forEach(key => {
    // if (typeof Value[key] === 'function') {
    helper.prototype[key + 'Value'] = function (objKey) {
        return Value[key](this.valueOf(), objKey);
    };
    // }
});

export default helper;
