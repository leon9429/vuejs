import Type from './type';
import Value from './value';
import Instance from './instance';

export default {
    value: Value,
    type: Type,
    instance: Instance
};
