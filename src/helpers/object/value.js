import Type from './type';

export default {
    fetch (obj, key) {
        if (Type.isObject(obj) && Type.isString(key)) {
            return (obj || {})[key];
        }
        return undefined;
    },
    find (obj, key) {
        if (Type.isObject(obj) && Type.isString(key)) {
            let result = this.fetch(obj, key);
            if (result === undefined && key.indexOf('.')) {
                let keys = key.split('.');
                let newkey = keys.splice(0, 1)[0];
                result = this.fetch(obj, newkey);
                if (result !== undefined) {
                    return this.find(result, keys.join('.'));
                } else {
                    while (result === undefined && keys.length) {
                        newkey += '.' + keys.splice(0, 1)[0];
                        result = this.fetch(obj, newkey);
                    }
                    return this.find(result, keys.join('.'));
                }
            }
            return result;
        }
        return undefined;
    }
};
