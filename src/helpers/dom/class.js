let helper = {
    has (el, name) {
        return (new RegExp('(\\s|^)' + name + '(\\s|$)')).test(el.className);
    },
    add (el, name) {
        if (!hasClass(el, name)) {
            el.className += ' ' + name;
        }
    },
    remove (el, name) {
        if (this.hasClass(el, name)) {
            var reg = new RegExp('(\\s|^)' + name + '(\\s|$)');
            el.className = el.className.replace(reg, ' ');
        }
    }
};

export default helper;
