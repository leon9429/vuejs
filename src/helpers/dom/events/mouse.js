import '@/helpers/array';
import PlaneHelper from '@/helpers/math/plane';

const STATUS = {
    NONE: 'none',
    READY: 'ready',
    CANCEL: 'cancel',
    START: 'start',
    CLICK: 'click',
    DCLICK: 'double-click',
    MCLICK: 'more-click',
    LCLICK: 'long-click',
    MOVING: 'moving',
    END: 'end',
    SWIPE: 'swipe',
    SWIPEUP: 'swipe-up',
    SWIPEDOWN: 'swipe-down',
    SWIPELEFT: 'swipe-left',
    SWIPERIGHT: 'swipe-right',
    SWIPENONE: 'swipe-none'
};

let __protected__ = {
    count: 0,
    actives: [],
    clicking: null,
    swiping: null,
    instances: {}
};

/**
 * 鼠标事件处理模块
 * @param  { HTMLElement } el HTML 元素对象
 * @param  { Object } options 配置
 */
const MouseEventHandler = function (el, options = {}) {
    if (!(el instanceof HTMLElement)) {
        throw new Error('元素不是「HTMLElement」。');
    } else if (!el.id) {
        el.id = 'me-' + (++__protected__.count);
    }
    if (__protected__[el.id]) {
        console.info('元素「' + el.id + '」已经存在。');
        return;
    }

    this.id = el.id;
    this.el = el;
    this.setOptions(options);

    this.el.addEventListener('mousedown', this.onStart.bind(this), false);
    this.el.addEventListener('mousemove', this.onMoving.bind(this), false);
    this.el.addEventListener('mouseup', this.onEnd.bind(this), false);
    this.el.addEventListener('mouseleave', this.onCancel.bind(this), false);

    this.init();
};
 
MouseEventHandler.prototype = {
    get instance () {
        return __protected__.instances[this.id];
    },
    get current () {
        return this.instance.current;
    },
    get points () {
        return this.instance.points;
    },
    get status () {
        return this.instance.status;
    },
    get clickable () {
        if (__protected__.swiping !== null) return false;
        if (__protected__.clicking && __protected__.clicking !== this.id) return false;

        return this.instance.status.hasnotValue(STATUS.MOVING) &&
            this.instance.status.hasnotValue(STATUS.LCLICK);
    },
    get moveable () {
        return this.status.hasValue(STATUS.START) &&
            this.status.hasnotValue(STATUS.END) &&
            [STATUS.START, STATUS.LCLICK, STATUS.MOVING].hasValue(this.status.last);
    },
    get movingable () {
        if (this.instance.current.points.first.distance > this.options.moveMomentum) {
            clearTimeout(this.instance.longTimer);
        }
        if (__protected__.swiping && __protected__.swiping !== this.id) {
            return false;
        }
        return true;
    },
    get swipeable () {
        if (__protected__.swiping && __protected__.swiping !== this.id) return false;

        let direction = this.instance.current.points.first.direction;
        if ((this.options.swipe.xable &&
                [PlaneHelper.const.directions.LEFT, PlaneHelper.const.directions.RIGHT].hasValue(direction)) ||
            (this.options.swipe.yable && [PlaneHelper.const.directions.UP, PlaneHelper.const.directions.DOWN].hasValue(direction))) {
            __protected__.swiping = this.id;
            return true;
        }
        return __protected__.swiping === this.id;
    },
    setOptions (options) {
        this.options = {
            data: null,
            clickTimeout: 100,
            longTimeout: 750,
            moveMomentum: 5,
            selfclick: false,
            ...this.options,
            ...options,
            ...options.mouse
        };

        // swipe 默认禁止
        this.options.swipe = {
            xable: options.swipe === true || (options.swipe && options.swipe.xable === true) || false,
            yable: options.swipe === true || (options.swipe && options.swipe.yable === true) || false
        };
    },
    init () {
        if (__protected__.instances[this.id]) {
            clearTimeout(__protected__.instances[this.id].longTimer);
            clearTimeout(__protected__.instances[this.id].clickTimer);
        }
        __protected__.instances[this.id] = {
            ...__protected__.instances[this.id],
            status: [STATUS.READY],
            points: []
        };
        __protected__.actives.removeValue(this.id);
        if (__protected__.actives.length === 0) {
            __protected__.swiping = null;
            __protected__.clicking = null;
        }
    },
    resolve (evt, by) {
        let self = this;
        let comparePoint = self.points.length
            ? self.points.first.first
            : {
                x: evt.pageX,
                y: evt.pageY,
                t: Date.now()
            };
        let point = {
            origin: evt,
            x: evt.pageX,
            y: evt.pageY,
            t: Date.now(),
            by: by || self.status.last,
            get offset () {
                return PlaneHelper.getTwoPointsOffset(comparePoint, point);
            },
            get distance () {
                return PlaneHelper.getTwoPointsDistance(comparePoint, point);
            },
            get direction () {
                return PlaneHelper.getTwoPointsDirection(comparePoint, point);
            },
            get position () {
                return PlaneHelper.getTwoPointsPosition(comparePoint, point);
            }
        };
        // 问：为什么不用 self.instance.points.last?
        // 答：在某些情况下「点」要先进行计算而不会存入记录中
        let points = [point];
        self.instance.current = { points };
        return points;
    },
    onStart (evt) {
        evt.preventDefault();

        let instance = __protected__.instances[this.id];
        clearTimeout(instance.clickTimer);
        if (instance.status.last === STATUS.READY) {
            __protected__.actives.push(this.id);
            instance.status.push(STATUS.START);
            instance.points.push(this.resolve(evt));
            this.trigger();
        }
        instance.longTimer = setTimeout(() => {
            instance.status.push(STATUS.LCLICK);
            this.trigger();
        }, this.options.longTimeout);
    },
    onMoving (evt) {
        evt.preventDefault();

        let instance = __protected__.instances[this.id];
        if (!this.moveable) {
            return;
        }

        let points = this.resolve(evt, STATUS.MOVING);
        if (this.movingable) {
            if (this.swipeable) {
                instance.status.push(STATUS.MOVING);
                instance.points.push(points);
                this.trigger();
            }
        }
    },
    onEnd (evt) {
        evt.preventDefault();

        if ([STATUS.READY].hasValue(this.status.last)) {
            return;
        }

        let instance = __protected__.instances[this.id];
        clearTimeout(instance.longTimer);

        if (instance.status.hasnotValue(STATUS.END)) {
            instance.status.push(STATUS.END);
            this.trigger();
        }
        instance.points.push(this.resolve(evt));

        if (this.clickable) {
            if (this.options.selfclick === true) {
                __protected__.clicking = this.id;
            }
            switch (this.status.last) { // 判断当前状态
                case STATUS.CLICK:
                    instance.status.push(STATUS.DCLICK);
                    instance.clickTimer = setTimeout(() => {
                        this.trigger();
                    }, this.options.clickTimeout);
                    break;
                case STATUS.DCLICK:
                    instance.status.push(STATUS.MCLICK);
                    instance.clickcount = 3;
                    instance.clickTimer = setTimeout(() => {
                        this.trigger({ data: { clickcount: instance.clickcount }});
                    }, this.options.clickTimeout);
                    break;
                case STATUS.MCLICK:
                    instance.clickcount++;
                    instance.clickTimer = setTimeout(() => {
                        this.trigger({ data: {clickcount: instance.clickcount }});
                    }, this.options.clickTimeout);
                    break;
                default:
                    instance.status.push(STATUS.CLICK);
                    instance.clickTimer = setTimeout(() => {
                        this.trigger();
                    }, this.options.clickTimeout);
            };
        } else {
            if (this.swipeable) {
                instance.status.push(STATUS.SWIPE);
                this.trigger();
                instance.status.push(STATUS['SWIPE' + instance.current.points.first.direction.toUpperCase()]);
                this.trigger();
            } else {
                instance.status.push(STATUS.NONE);
                this.trigger();
            }
        }
    },
    onCancel (evt) {
        evt.preventDefault();

        if ([STATUS.READY, STATUS.END].hasValue(this.status.last)) {
            return;
        }
        this.status.push(STATUS.CANCEL);
        this.trigger();
    },
    trigger () {
        if (this.status.last === STATUS.READY) {
            return;
        }
        let info = {
            name: this.status.last,
            el: this.el,
            points: [...this.points],
            status: [...this.status],
            data: this.options.data
        };
        let delegateName = 'on' + this.instance.status.last.replace(/\b\w+\b/g, s => { return s.substring(0, 1).toUpperCase() + s.substring(1); }).replace('-', '');
        if (typeof this.options[delegateName] === 'function') {
            this.options[delegateName](info);
        }
        if (this.options.debug === true) {
            console.warn(delegateName + ' not impletement.', { ...info });
        }

        if ([STATUS.READY, STATUS.START, STATUS.END, STATUS.LCLICK, STATUS.MOVING, STATUS.SWIPE].hasnotValue(this.status.last) ||
            (this.status.last === STATUS.END && [STATUS.MOVING, STATUS.LCLICK].hasValue(this.status.valueAt(-2))) &&
                this.swipeable === false) {
            // moving 和 end 单独不会初始化，但是 moving + end 需要初始化(且触发状态不是 swipe)
            this.init();
        }
    }
};

export default MouseEventHandler;
