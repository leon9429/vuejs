import MouseEventHandler from './mouse';
import TouchEventHandler from './touch';

export default {
    MouseEventHandler,
    TouchEventHandler
};
