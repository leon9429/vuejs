import '@/helpers/array';
import PlaneHelper from '@/helpers/math/plane';

const STATUS = {
    NONE: 'none',
    READY: 'ready',
    CANCEL: 'cancel',
    START: 'start',
    CLICK: 'click',
    DCLICK: 'double-click',
    MCLICK: 'more-click',
    LCLICK: 'long-click',
    MOVING: 'moving',
    END: 'end',
    SWIPE: 'swipe',
    SWIPEUP: 'swipe-up',
    SWIPEDOWN: 'swipe-down',
    SWIPELEFT: 'swipe-left',
    SWIPERIGHT: 'swipe-right',
    PINCH: 'pinch',
    ROTATE: 'rotate'
};

let __protected__ = {
    count: 0,
    actives: [],
    clicking: null,
    swiping: null,
    instances: {}
};

/**
 * 鼠标事件处理模块
 * @param  { HTMLElement } el HTML 元素对象
 * @param  { Object } options 配置
 */
const TouchEventHandler = function (el, options = {}) {
    if (!(el instanceof HTMLElement)) {
        throw new Error('元素不是「HTMLElement」。');
    } else if (!el.id) {
        el.id = 'me-' + (++__protected__.count);
    }
    if (__protected__[el.id]) {
        console.info('元素「' + el.id + '」已经存在。');
        return;
    }

    this.id = el.id;
    this.el = el;
    this.setOptions(options);

    this.el.addEventListener('touchstart', this.onStart.bind(this), false);
    this.el.addEventListener('touchmove', this.onMoving.bind(this), false);
    this.el.addEventListener('touchend', this.onEnd.bind(this), false);
    this.el.addEventListener('touchcancel', this.onCancel.bind(this), false);

    this.init();
};

TouchEventHandler.prototype = {
    get instance () {
        return __protected__.instances[this.id];
    },
    get current () {
        return this.instance.current;
    },
    get points () {
        return this.instance.points;
    },
    get status () {
        return this.instance.status;
    },
    get clickable () {
        if (__protected__.swiping !== null) return false;
        if (__protected__.clicking && __protected__ !== this.id) return false;

        return this.current.points.length === 1 &&
            this.instance.status.hasnotValue(STATUS.MOVING) &&
            this.instance.status.hasnotValue(STATUS.LCLICK);
    },
    get longable () {
        return this.current.points.length === 1;
    },
    get moveable () {
        return this.status.hasValue(STATUS.START) &&
            this.status.hasnotValue(STATUS.END) &&
            [STATUS.START, STATUS.LCLICK, STATUS.MOVING, STATUS.PINCH, STATUS.ROTATE].hasnotValue(this.status.last);
    },
    get movingable () {
        if (PlaneHelper.getTwoPointsDistance(this.points.last.first, this.current.points.first) > this.options.moveMomentum ||
            PlaneHelper.getTwoPointsDistance(this.points.last.last, this.current.points.last) > this.options.moveMomentum) {
            clearTimeout(this.instance.longTimer);
        }
        if (__protected__.swiping && __protected__.swiping !== this.id) {
            return false;
        }
        return true;
    },
    get swipeable () {
        if (__protected__.swiping && __protected__.swiping !== this.id) return false;

        let direction = this.current.points.first.direction;
        if ((this.options.swipe.xable &&
                [PlaneHelper.const.directions.LEFT, PlaneHelper.const.directions.RIGHT].hasValue(direction)) ||
            (this.options.swipe.yable && [PlaneHelper.const.directions.UP, PlaneHelper.const.directions.DOWN].hasValue(direction))) {
            __protected__.swiping = this.id;
            return true;
        }
        return __protected__.swiping === this.id;
    },
    setOptions (options) {
        this.options = {
            data: null,
            clickTimeout: 150,
            longTimeout: 750,
            moveMomentum: 15,
            selfclick: false,
            ...this.options,
            ...options,
            ...options.touch
        };

        // swipe 默认禁止
        this.options.swipe = {
            xable: options.swipe === true || (options.swipe && options.swipe.xable === true) || false,
            yable: options.swipe === true || (options.swipe && options.swipe.yable === true) || false
        };
    },
    init () {
        if (__protected__.instances[this.id]) {
            clearTimeout(__protected__.instances[this.id].longTimer);
            clearTimeout(__protected__.instances[this.id].clickTimer);
        }
        __protected__.instances[this.id] = {
            ...__protected__.instances[this.id],
            status: [STATUS.READY],
            points: []
        };
        __protected__.actives.removeValue(this.id);
        if (__protected__.actives.length === 0) {
            __protected__.swiping = null;
            __protected__.clicking = null;
        }
    },
    resolve (evt, by) {
        let self = this;
        let touches = evt.touches.length ? evt.touches : evt.changedTouches;
        let points = Object.keys(touches).map((key, index) => {
            let comparePoint = self.points.length
                ? self.points.first[index]
                : {
                    x: touches[key].pageX,
                    y: touches[key].pageY,
                    t: Date.now()
                };
            let point = {
                origin: touches[key],
                x: touches[key].pageX,
                y: touches[key].pageY,
                t: Date.now(),
                by: by || self.status.last,
                get offset () {
                    return PlaneHelper.getTwoPointsOffset(comparePoint, point);
                },
                get distance () {
                    return PlaneHelper.getTwoPointsDistance(comparePoint, point);
                },
                get direction () {
                    return PlaneHelper.getTwoPointsDirection(comparePoint, point);
                },
                get position () {
                    return PlaneHelper.getTwoPointsPosition(comparePoint, point);
                }
            };
            return point;
        });
        self.instance.current = { points };
        return points;
    },
    onStart (evt) {
        evt.preventDefault();

        let instance = __protected__.instances[this.id];
        clearTimeout(instance.clickTimer);
        let points = this.resolve(evt, STATUS.START);
        if (instance.status.last === STATUS.READY) {
            __protected__.actives.push(this.id);
            instance.status.push(STATUS.START);
            instance.points.push(points);
            this.trigger();
        }

        if (this.longable) {
            instance.longTimer = setTimeout(() => {
                instance.status.push(STATUS.LCLICK);
                this.trigger();
            }, this.options.longTimeout);
        } else {
            // 取消所有长按计时
            Object.keys(__protected__.instances).forEach(id => {
                clearTimeout(__protected__.instances[id].longTimer);
            });
        }
    },
    onMoving (evt) {
        evt.preventDefault();

        let instance = __protected__.instances[this.id];
        if (this.moveable) {
            return;
        }

        let points = this.resolve(evt, STATUS.MOVING);
        if (this.movingable) {
            instance.points.push(points);
            if (points.length === 1) {
                if (this.swipeable) {
                    instance.status.push(STATUS.MOVING);
                    this.trigger();
                }
            } else if (points.length === 2) {
                if (points.first.origin.target === points.last.origin.target || 
                    (document.querySelectorAll('#' + this.id + ' #' + points.first.origin.target.id).length === 1 &&
                        document.querySelectorAll('#' + this.id + ' #' + points.last.origin.target.id).length === 1)) {
                    let beginPoints = instance.points.filter(pv => {
                        return pv.length === 2;
                    }).first;
                    let offsetFrom = {
                        x: beginPoints.last.x - beginPoints.first.x,
                        y: beginPoints.last.y - beginPoints.first.y
                    };
                    let offsetTo = {
                        x: points.last.x - points.first.x,
                        y: points.last.y - points.first.y
                    };
                    let scale = PlaneHelper.getTwoVectorsScale(offsetTo, offsetFrom);
                    instance.status.push(STATUS.PINCH);
                    this.trigger({ data: { scale }});

                    let rotate = PlaneHelper.getTwoVectorsAngle(offsetTo, offsetFrom);
                    instance.status.push(STATUS.ROTATE);
                    this.trigger({ data: { rotate }});
                }
            } else {
                console.log(points.length + ' touches moving');
            }
        }
    },
    onEnd (evt) {
        evt.preventDefault();

        if ([STATUS.READY].hasValue(this.status.last)) {
            return;
        }

        let instance = __protected__.instances[this.id];
        clearTimeout(instance.longTimer);

        if (instance.status.hasnotValue(STATUS.END)) {
            instance.status.push(STATUS.END);
            this.trigger();
        }

        let points = this.resolve(evt);
        instance.points.push(points);

        if (this.clickable) {
            if (this.options.selfclick === true) {
                __protected__.clicking = this.id;
            }
            switch (this.status.last) { // 判断当前状态
                case STATUS.CLICK:
                    instance.status.push(STATUS.DCLICK);
                    instance.clickTimer = setTimeout(() => {
                        this.trigger();
                    }, this.options.clickTimeout);
                    break;
                case STATUS.DCLICK:
                    instance.status.push(STATUS.MCLICK);
                    instance.clickcount = 3;
                    instance.clickTimer = setTimeout(() => {
                        this.trigger();
                    }, this.options.clickTimeout);
                    break;
                case STATUS.MCLICK:
                    instance.clickcount++;
                    instance.clickTimer = setTimeout(() => {
                        this.trigger();
                    }, this.options.clickTimeout);
                    break;
                default:
                    instance.status.push(STATUS.CLICK);
                    instance.clickTimer = setTimeout(() => {
                        this.trigger();
                    }, this.options.clickTimeout);
            };
        } else if (points.length === 1) {
            if (this.swipeable) {
                instance.status.push(STATUS.SWIPE);
                this.trigger();
                instance.status.push(STATUS['SWIPE' + instance.current.points.first.direction.toUpperCase()]);
                this.trigger();
            } else {
                instance.status.push(STATUS.NONE);
                this.trigger();
            }
        }
    },
    onCancel (evt) {
        evt.preventDefault();

        if ([STATUS.READY, STATUS.END].hasValue(this.status.last)) {
            return;
        }
        this.status.push(STATUS.CANCEL);
        this.trigger();
    },
    trigger () {
        let instance = __protected__.instances[this.id];
        if (instance.status.last === STATUS.READY) {
            return;
        }
        let info = {
            name: instance.status.last,
            el: this.el,
            points: [...instance.points],
            status: [...instance.status],
            data: this.options.data
        };
        let delegateName = 'on' + instance.status.last.replace(/\b\w+\b/g, s => { return s.substring(0, 1).toUpperCase() + s.substring(1); }).replace('-', '');
        if (typeof this.options[delegateName] === 'function') {
            this.options[delegateName](info);
        }
        if (this.options.debug === true) {
            console.warn(delegateName + ' not impletement.', { ...info });
        }
        if ([STATUS.START, STATUS.END, STATUS.LCLICK, STATUS.MOVING, STATUS.SWIPE, STATUS.PINCH, STATUS.ROTATE].hasnotValue(instance.status.last) ||
            (instance.status.last === STATUS.END && [STATUS.LCLICK, STATUS.MOVING].hasValue(instance.status.valueAt(-2)) &&
                this.swipeable === false)) {
            // moving 和 end 单独不会初始化，但是 moving + end 需要初始化(且触发状态不是 swipe)
            this.init();
        }
    }
};

export default TouchEventHandler;
