import PlaneHelper from '@/helpers/math/plane';

let HandlerManager = function (el) {
    this.handlers = [];
    this.el = el;
};
HandlerManager.prototype.add = function (handler) {
    this.handlers.push(handler);
};
HandlerManager.prototype.del = function (handler) {
    if (!handler) this.handlers = [];

    let delIndex = this.handlers.findIndex(h => h === handler);
    if (delIndex >= 0) this.handlers.splice(delIndex, 1);
};
HandlerManager.prototype.dispatch = function () {
    this.handlers.forEach(handler => {
        if (typeof handler === 'function') handler.apply(this.el, arguments);
    });
};

let wrapFunc = (el, handler) => {
    let manager = new HandlerManager(el);
    manager.add(handler);
    return manager;
};

let count = 0;
let Finger = function (el, options) {
    this.element = typeof el === 'string' ? document.querySelector(el) : el;
    this.id = this.element.id || ++count;
    el.id = this.id; // 元素 id 同步
    this.options = Object.assign({
        data: null, // 绑定到这个对象的数据（用于数据传递）
        offsetX: 0,
        offsetY: 0,
        clickTimeout: 150, // 点击触发时间 (ms)
        longclickTimeout: 750, // 长按触发时间 (ms)
        moveMomentum: 5 // 移动量
    }, options);
    this.onStart = this.onStart.bind(this);
    this.onMoving = this.onMoving.bind(this);
    this.onEnd = this.onEnd.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.usingMouse = false;
    if (window.ontouchstart === undefined) {
        this.usingMouse = true;
        this.element.addEventListener('mousedown', this.onStart, false);
        this.element.addEventListener('mousemove', this.onMoving, false);
        this.element.addEventListener('mouseup', this.onEnd, false);
        this.element.addEventListener('mouseleave', this.onCancel, false);
    } else {
        this.element.addEventListener('touchstart', this.onStart, false);
        this.element.addEventListener('touchmove', this.onMoving, false);
        this.element.addEventListener('touchend', this.onEnd, false);
        this.element.addEventListener('touchcancel', this.onCancel, false);
    }
    this.change = wrapFunc(this.element, options.onChange); // 事件变更（每一个事件）
    this.handle = wrapFunc(this.element, options.onHandle); // 操作事件
    // 基础事件
    this.start = wrapFunc(this.element, options.onStart); // 开始
    this.end = wrapFunc(this.element, options.onEnd); // 结束
    this.moving = wrapFunc(this.element, options.onMoving); // 移动
    this.cancel = wrapFunc(this.element, options.onCancel); // 中断
    // 点击事件
    this.click = wrapFunc(this.element, options.onClick || options.onTap); // 有点击
    this.singleClick = wrapFunc(this.element, options.onOneClick || options.onOneTap); // 单击
    this.doubleClick = wrapFunc(this.element, options.onDoubleClick || options.onDoubleTap); // 双击
    this.moreClick = wrapFunc(this.element, options.onMoreClick || options.onMoreTap); // 多击
    this.longClick = wrapFunc(this.element, options.onLongClick || options.onLongTap); // 长击
    // 移动事件
    this.swipe = wrapFunc(this.element, options.onSwipe); // 滑动
    this.swipeUp = wrapFunc(this.element, options.onSwipeUp); //
    this.swipeDown = wrapFunc(this.element, options.onSwipeDown);
    this.swipeLeft = wrapFunc(this.element, options.onSwipeLeft);
    this.swipeRight = wrapFunc(this.element, options.onSwipeRight);
    // 多指事件
    this.pinch = wrapFunc(this.element, options.onPinch);
    this.rotate = wrapFunc(this.element, options.onRotate);
    // 状态初始化
    this.init();
};
Finger.prototype = {
    init () {
        this.isMoving = false;
        this.isMoveable = false;
        this.points = [];
        this.clickcount = 0;
        this.clickTimeout = setTimeout(() => {});
        this.isLongClick = false;
        this.longclickTimeout = setTimeout(() => {});
    },
    computePointsDelta (from, to) {
        let offsetX = to.x - from.x;
        let offsetY = to.y - from.y;
        return Object.assign(to, {
            offset: {
                x: offsetX,
                y: offsetY,
                t: to.t - from.t
            }
        });
    },
    computePointsMathData (from, to) {
        let offsetX = to.x - from.x;
        let offsetY = to.y - from.y;
        return {
            directions: {
                n: offsetY < 0,
                s: offsetY > 0,
                w: offsetX < 0,
                e: offsetX > 0
            },
            direction: Math.abs(offsetX) >= Math.abs(offsetY) ? (offsetX < 0 ? 'Left' : 'Right') : (offsetY < 0 ? 'Up' : 'Down'),
            angle: PlaneHelper.getTwoPointsAngle(from, to),
            distance: PlaneHelper.getTwoPointsDistance(from, to)
        };
    },
    setNumberPrecision (value) {
        let result = value;
        result = value.toFixed(0);
        return parseFloat(result);
    },
    setPoints (evt) {
        let points = [];
        let targets = [];
        if (this.usingMouse) {
            targets.push(evt.target.id);
            points.push({
                x: this.setNumberPrecision(evt.pageX + this.options.offsetX),
                y: this.setNumberPrecision(evt.pageY + this.options.offsetY),
                t: Date.now()
            });
        } else {
            let touches = evt.touches.length ? evt.touches : evt.changedTouches;
            Object.keys(touches).forEach((key, index) => {
                targets.push(touches[key].target.id);
                points.push({
                    x: this.setNumberPrecision(touches[key].pageX + this.options.offsetX),
                    y: this.setNumberPrecision(touches[key].pageY + this.options.offsetY),
                    t: Date.now()
                });
            });
        }
        points.forEach((point, index) => {
            if (!this.points[index]) {
                this.points.push({ from: point, to: this.computePointsDelta(point, point), target: targets[index] });
            } else {
                let from = this.points[index].from;
                point = Object.assign({}, this.computePointsDelta(from, point));
                this.points[index] = Object.assign(this.points[index], this.computePointsMathData(from, point));
                this.points[index].to = point;
            }
        });
    },
    onStart (evt) {
        evt.preventDefault();
        this.setPoints(evt);
        if (this.points.length > 1) {
            // 多点
            clearTimeout(this.longclickTimeout);
        } else {
            // 单点
            this.longclickTimeout = setTimeout(() => {
                this.trigger('longClick', evt);
                this.isLongClick = true;
            }, this.options.longclickTimeout);
            this.clickcount++;
            if (this.points.length) {
                // 判断第二次点击与第一次的事件间隔，决定是否取消之前的单机事件。
                if (this.points[0].to.offset.t < this.options.clickTimeout) {
                    clearTimeout(this.clickTimeout);
                }
            }
        }
        this.isMoveable = true;
        this.trigger('start', evt);
    },
    onMoving (evt) {
        evt.preventDefault();
        if (this.points.length === 0) return;
        if (!this.isMoveable) return;

        this.setPoints(evt);
        if (!this.isMoving) {
            let movingPoints = this.points.filter(point => {
                let momentum = (Math.abs(point.to.offset.x) + Math.abs(point.to.offset.y)) / 2;
                return momentum > this.options.moveMomentum;
            });
            if (movingPoints.length === 0) {
                return;
            }
        }

        clearTimeout(this.longclickTimeout);
        this.clickcount = 0;
        this.isMoving = true;
        if (this.points.length === 1) {
            this.trigger('moving', evt);
        } else if (this.points.length === 2) {
            let offsetFrom = {
                x: this.points[1].from.x - this.points[0].from.x,
                y: this.points[1].from.y - this.points[0].from.y
            };
            let offsetTo = {
                x: this.points[1].to.x - this.points[0].to.x,
                y: this.points[1].to.y - this.points[0].to.y
            };

            let scale = PlaneHelper.getTwoVectorsScale(offsetTo, offsetFrom);
            this.trigger('pinch', evt, { scale });

            let rotate = PlaneHelper.getTwoVectorsAngle(offsetTo, offsetFrom);
            this.trigger('rotate', evt, { rotate });
        }
    },
    onEnd (evt) {
        evt.preventDefault();
        if (this.points.length === 0) return;
        clearTimeout(this.longclickTimeout);
        this.setPoints(evt);
        this.trigger('end', evt);
        if (!this.isMoving) {
            if (!this.isLongClick) {
                this.clickTimeout = ((points, clickcount) => {
                    return setTimeout(() => {
                        points[0].clickcount = clickcount;
                        switch (clickcount) {
                            case 1:
                                this.trigger('singleClick', evt, { points });
                                break;
                            case 2:
                                this.trigger('doubleClick', evt, { points });
                                break;
                            default:
                                this.trigger('moreClick', evt, { points });
                        };
                        this.trigger('click', evt, { points });
                    }, this.options.clickTimeout);
                })(this.points, this.clickcount);
            }
        } else if (this.points.length === 1) {
            this.trigger('swipe', evt);
            this.trigger('swipe' + this.points[0].direction, evt);
        }
        this.isMoving = false;
        this.isMoveable = false;
        this.isLongClick = false;
        this.points = [];
    },
    onCancel (evt) {
        evt.preventDefault();
        if (this.points.length === 0) return;
        this.trigger('cancel', evt);
    },
    trigger (name, evt, evtdata) {
        if (!this[name] || !this[name].dispatch || typeof this[name].dispatch !== 'function') {
            return;
        }
        let info = Object.assign({ name, data: this.options.data, el: this.element, points: this.points }, evtdata);
        this[name].dispatch(evt, info);
        this.change.dispatch(evt, info);
        // if (['click', 'singleClick', 'doubleClick', 'moreClick', 'longClick', 'moving', 'swipe', 'swipeUp', 'swipeDown', 'swipeLeft', 'swipeRight', 'pinch', 'rotate'].indexOf(name) !== -1) {
        //     // 确定的操作事件
        //     this.handle.dispatch(evt, info);
        // }
        if (['start', 'end', 'moving', 'swipe', 'click', 'longClick', 'pinch', 'rotate'].indexOf(name) === -1) {
            // 不初始化的事件
            // this.trigger('end', evt, Object.assign(info, { name: name + '.end' }));
            this.init();
        }
    },
    on (eventName, handler) {
        if (typeof this[eventName] === 'function') {
            this[eventName].add(handler);
        }
    },
    off (eventName, handler) {
        if (typeof this[eventName] === 'function') {
            this[eventName].del(handler);
        }
    }
};

export default Finger;
