import '@/helpers/string';
import '@/helpers/array';
import Events from './events';

let __instances__ = {};
const __private = {
    init (instance) {
        if (window.ontouchstart === undefined) {
            __instances__[instance.id] = new Events.MouseEventHandler(instance.el, instance.options);
        } else {
            __instances__[instance.id] = new Events.TouchEventHandler(instance.el, instance.options);
        }
    }
};

let count = 0;
const MT = function (elementOrSelector, options) {
    if (!this) {
        return new MT(elementOrSelector, options);
    }
    let $el = typeof elementOrSelector === 'string'
        ? document.querySelector(elementOrSelector)
        : elementOrSelector;
    if ($el instanceof HTMLElement === false) {
        throw new Error('Can not bind a target that is not [HTMLElement].');
    }
    if (!$el.id) {
        $el.id = 'mt-' + (++count);
    }

    this.id = $el.id;
    this.el = $el;
    this.options = options;

    __private.init(this);
};

MT.prototype = {
    setOptions (options) {
        __instances__[this.id].setOptions(options);
    }
};

export default MT;
