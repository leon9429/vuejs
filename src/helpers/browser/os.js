const helper = {
    /**
     * 只读
     * @return {String} 返回操作系统信息
     */
    get info () {
        let userAgent = 'navigator' in window && 'userAgent' in navigator && navigator.userAgent.toLowerCase() || '';
        let appVersion = 'navigator' in window && 'appVersion' in navigator && navigator.appVersion.toLowerCase() || '';
        if (/iphone/i.test(userAgent) || /ipad/i.test(userAgent) || /ipod/i.test(userAgent)) return 'ios';
        if (/android/i.test(userAgent)) return 'android';
        if (/win/i.test(appVersion) && /phone/i.test(userAgent)) return 'winphone';
        if (/mac/i.test(appVersion)) return 'mac';
        if (/win/i.test(appVersion)) return 'windows';
        if (/linux/i.test(appVersion)) return 'linux';
        return 'unknow';
    }
};

export default helper;
