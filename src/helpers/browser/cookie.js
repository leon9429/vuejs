/**
 * Cookie 操作
 * @see https://developer.mozilla.org/zh-CN/docs/Web/API/Document/cookie
 * @type {Object}
 * - @function get(key)
 * - @function set()
 */
const helper = {
    get (key) {
        return decodeURIComponent(document.cookie.replace(new RegExp('(?:(?:^|.*;)\\s*' + encodeURIComponent(key).replace(/[-.+*]/g, '\\$&') + '\\s*\\=\\s*([^;]*).*$)|^.*$'), '$1')) || null;
    },
    set (key, value, end, path, domain, secure) {
        if (!key || /^(?:expires|max-age|path|domain|secure)$/i.test(key)) { return false; }
        let expires = '';
        if (end) {
            switch (end.constructor) {
                case Number:
                    expires = end === Infinity ? '; expires=Fri, 31 Dec 9999 23:59:59 GMT' : '; max-age=' + end;
                    break;
                case String:
                    expires = '; expires=' + end;
                    break;
                case Date:
                    expires = '; expires=' + end.toUTCString();
                    break;
            }
        }
        document.cookie = encodeURIComponent(key) + '=' + encodeURIComponent(value) + expires + (domain ? '; domain=' + domain : '') + (path ? '; path=' + path : '') + (secure ? '; secure' : '');
        return true;
    },
    remove (key, path, domain) {
        if (!key || !this.has(key)) { return false; }
        document.cookie = encodeURIComponent(key) + '=; expires=Thu, 01 Jan 1970 00:00:00 GMT' + (domain ? '; domain=' + domain : '') + (path ? '; path=' + path : '');
        return true;
    },
    has (key) {
        return (new RegExp('(?:^|;\\s*)' + encodeURIComponent(key).replace(/[-.+*]/g, '\\$&') + '\\s*\\=')).test(document.cookie);
    },
    get keys () {
        let keys = document.cookie.replace(/((?:^|\s*;)[^=]+)(?=;|$)|^\s*|\s*(?:=[^;]*)?(?:\1|$)/g, '').split(/\s*(?:=[^;]*)?;\s*/);
        if (!keys[0]) {
            return [];
        }
        for (let i = 0; i < keys.length; i++) { keys[i] = decodeURIComponent(keys[i]); }
        return keys;
    },
    get items () {
        let items = [];
        this.keys.forEach(key => {
            items.push({ key, value: this.get(key) });
        });
        return items;
    }
};

export default helper;
