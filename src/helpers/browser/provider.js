// 各浏览器的名称及对应正则表达式
let patterns = [{
    name: 'ie',
    value: /rv:([\d.]+)\) like gecko|msie ([\d.]+)/i
}, {
    name: 'edge',
    value: /edge\/([\d.]+)/i
}, {
    name: 'firefox',
    value: /firefox\/([\d.]+)/i
}, {
    name: 'opera',
    value: /(?:opera|opr).([\d.]+)/i
}, {
    name: 'chrome',
    value: /chrome\/([\d.]+)/i
}, {
    name: 'safari',
    value: /version\/([\d.]+).*safari/i
}];

const helper = {
    /**
     * 只读
     * @return {Object} 返回浏览器信息
     *  - @property {String} name 名字
     *  - @property {String} code 版本号
     *  - @property {Number} number 版本数
     */
    get info () {
        let userAgent = 'navigator' in window && 'userAgent' in navigator && navigator.userAgent.toLowerCase() || '';
        let info = {};
        let pattern = [];
        while ((pattern = patterns.splice(0, 1)[0])) {
            let matches = userAgent.match(pattern.value);
            if (matches) {
                let code = matches[1];
                info = {
                    name: pattern.name,
                    code,
                    number: parseInt(code)
                };
                break;
            }
        }
        return info;
    }
};

export default helper;
