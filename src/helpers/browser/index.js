import OsHelper from './os';
import ProviderHelper from './provider';
import CookieHelper from './cookie';

export default {
    get os () {
        return OsHelper.info;
    },
    get provider () {
        return ProviderHelper.info;
    },
    get cookie () {
        return CookieHelper;
    }
};
