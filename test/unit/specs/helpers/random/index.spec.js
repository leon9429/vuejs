import RandompHelper from '@/helpers/random';
import RegexpHelper from '@/helpers/regexp';

describe('@/helpers/random/index', () => {
    it('number.max => <= 100', () => {
        expect(RandompHelper.number.max(100) <= 100).to.equal(true);
    });
    it('number.range => 50 - 100', () => {
        let value = RandompHelper.number.range(50, 100);
        expect(value >= 50 && value <= 100).to.equal(true);
    });
    it('r => <= 255', () => {
        expect(RandompHelper.color.r <= 255).to.equal(true);
    });
    it('g => <= 255', () => {
        expect(RandompHelper.color.g <= 255).to.equal(true);
    });
    it('b => <= 255', () => {
        expect(RandompHelper.color.b <= 255).to.equal(true);
    });
    it('rgb', () => {
        expect(RegexpHelper.isColor(RandompHelper.color.rgb)).to.equal(true);
    });
    it('rgba', () => {
        expect(RegexpHelper.isColor(RandompHelper.color.rgba)).to.equal(true);
    });
    it('hex', () => {
        expect(RegexpHelper.isColor(RandompHelper.color.hex)).to.equal(true);
    });
});
