import InstanceHelper from '@/helpers/object/instance';

describe('@/helpers/objec/instance', () => {
    it('number test', () => {
        let instance = new InstanceHelper(1);
        expect(instance.isNumber()).to.equal(true);
        expect(instance.isString()).to.equal(false);
        expect(instance.isBoolean()).to.equal(false);
        expect(instance.isArray()).to.equal(false);
        expect(instance.isObject()).to.equal(false);
        expect(instance.isNull()).to.equal(false);
        expect(instance.isUndefined()).to.equal(false);
        expect(instance.isEmpty()).to.equal(false);

        expect(instance + 1).to.equal(instance.value + 1);
        expect(instance.value).to.equal(instance.valueOf());
        expect(instance.toString()).to.equal(instance.value.toString());
    });
    it('string test', () => {
        let instance = new InstanceHelper('t');
        expect(instance.isNumber()).to.equal(false);
        expect(instance.isString()).to.equal(true);
        expect(instance.isBoolean()).to.equal(false);
        expect(instance.isArray()).to.equal(false);
        expect(instance.isObject()).to.equal(false);
        expect(instance.isNull()).to.equal(false);
        expect(instance.isUndefined()).to.equal(false);
        expect(instance.isEmpty()).to.equal(false);

        expect(instance + 'est').to.equal(instance.value + 'est');
    });
    it('empty string test', () => {
        let instance = new InstanceHelper('');
        expect(instance.isNumber()).to.equal(false);
        expect(instance.isString()).to.equal(true);
        expect(instance.isBoolean()).to.equal(false);
        expect(instance.isArray()).to.equal(false);
        expect(instance.isObject()).to.equal(false);
        expect(instance.isNull()).to.equal(false);
        expect(instance.isUndefined()).to.equal(false);
        expect(instance.isEmpty()).to.equal(true);

        expect(instance + 'est').to.equal(instance.value + 'est');
    });
    it('object test', () => {
        let instance = new InstanceHelper({});
        expect(instance.isNumber()).to.equal(false);
        expect(instance.isString()).to.equal(false);
        expect(instance.isBoolean()).to.equal(false);
        expect(instance.isArray()).to.equal(false);
        expect(instance.isObject()).to.equal(true);
        expect(instance.isNull()).to.equal(false);
        expect(instance.isUndefined()).to.equal(false);
        expect(instance.isEmpty()).to.equal(true);

        instance.value = { name: 'test', type: 'object', try: { name: 'test-try' } };
        expect(instance.isEmpty()).to.equal(false);
        expect(instance.fetchValue('name')).to.equal(instance.value.name);
        expect(instance.findValue('try.name')).to.equal(instance.value.try.name);

        expect(instance.toString()).to.equal(JSON.stringify(instance.value));
    });
    it('null test', () => {
        let instance = new InstanceHelper(null);
        expect(instance.isNumber()).to.equal(false);
        expect(instance.isString()).to.equal(false);
        expect(instance.isBoolean()).to.equal(false);
        expect(instance.isArray()).to.equal(false);
        expect(instance.isObject()).to.equal(true);
        expect(instance.isNull()).to.equal(true);
        expect(instance.isUndefined()).to.equal(false);
        expect(instance.isEmpty()).to.equal(true);
    });
    it('boolean test', () => {
        let instance = new InstanceHelper(true);
        expect(instance.isNumber()).to.equal(false);
        expect(instance.isString()).to.equal(false);
        expect(instance.isBoolean()).to.equal(true);
        expect(instance.isArray()).to.equal(false);
        expect(instance.isObject()).to.equal(false);
        expect(instance.isNull()).to.equal(false);
        expect(instance.isUndefined()).to.equal(false);
        expect(instance.isEmpty()).to.equal(false);
    });
    it('array test', () => {
        let instance = new InstanceHelper([]);
        expect(instance.isNumber()).to.equal(false);
        expect(instance.isString()).to.equal(false);
        expect(instance.isBoolean()).to.equal(false);
        expect(instance.isArray()).to.equal(true);
        expect(instance.isObject()).to.equal(false);
        expect(instance.isNull()).to.equal(false);
        expect(instance.isUndefined()).to.equal(false);
        expect(instance.isEmpty()).to.equal(true);
    });
});
