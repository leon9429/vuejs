import ValueHelper from '@/helpers/object/value';

describe('@/helpers/objec/value', () => {
    it('fetch undefined value', () => {
        expect(ValueHelper.fetch('this is string', null)).to.equal(undefined);
        expect(ValueHelper.fetch(null, '')).to.equal(undefined);
        expect(ValueHelper.fetch(null, null)).to.equal(undefined);
    });

    it('fetch normal value', () => {
        expect(ValueHelper.fetch({ name: 'test' }, 'a')).to.equal(undefined);
        expect(ValueHelper.fetch({ name: 'test' }, 'name')).to.equal('test');
    });
    
    let obj = {
        'test.name': 'testname',
        name: 'test',
        deep: {
            level: {
                name: 'level1',
                'info.name': {
                    string: 'uuuu'
                }
            },
            'name.string': 'deepvalue'
        }
    };

    it('find undefined value', () => {
        expect(ValueHelper.find('test', null)).to.equal(undefined);
        expect(ValueHelper.find(null, '')).to.equal(undefined);
        expect(ValueHelper.find(null, null)).to.equal(undefined);
    });

    it('find normal value', () => {
        expect(ValueHelper.find(obj, 'test')).to.equal(undefined);
        expect(ValueHelper.find(obj, 'name')).to.equal('test');
    });

    it('find deep value', () => {
        // 第二层对象已经不存在了。。。
        expect(ValueHelper.find(obj, 'deep.level.name.string')).to.equal(obj.level);
        expect(ValueHelper.find(obj, 'deep.level')).to.equal(obj.deep.level);
    });

    it('find mixed value', () => {
        expect(ValueHelper.find(obj, 'test.name')).to.equal(obj['test.name']);
        expect(ValueHelper.find(obj, 'deep.name.string')).to.equal(obj.deep['name.string']);
        expect(ValueHelper.find(obj, 'deep.level.info.name.string')).to.equal(obj.deep.level['info.name'].string);
    });
});
