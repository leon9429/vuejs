import TypeHelper from '@/helpers/object/type';

describe('@/helpers/objec/type', () => {
    it('check string type', () => {
        expect(TypeHelper.isString('this is string')).to.equal(true);
        expect(TypeHelper.isString([])).to.equal(false);
        expect(TypeHelper.isString(1)).to.equal(false);
        expect(TypeHelper.isString({})).to.equal(false);
    });

    it('check number type', () => {
        expect(TypeHelper.isNumber('this is string')).to.equal(false);
        expect(TypeHelper.isNumber([])).to.equal(false);
        expect(TypeHelper.isNumber(1)).to.equal(true);
        expect(TypeHelper.isNumber({})).to.equal(false);
        expect(TypeHelper.isNumber('12')).to.equal(true);
        expect(TypeHelper.isNumber('12.34')).to.equal(true);
        expect(TypeHelper.isNumber('12.2.43')).to.equal(false);
    });

    it('check array type', () => {
        expect(TypeHelper.isArray('this is string')).to.equal(false);
        expect(TypeHelper.isArray([])).to.equal(true);
        expect(TypeHelper.isArray(1)).to.equal(false);
        expect(TypeHelper.isArray({})).to.equal(false);
    });

    it('check object type', () => {
        expect(TypeHelper.isObject('this is string')).to.equal(false);
        expect(TypeHelper.isObject([])).to.equal(false);
        expect(TypeHelper.isObject(1)).to.equal(false);
        expect(TypeHelper.isObject({})).to.equal(true);
    });

    it('check bool type', () => {
        expect(TypeHelper.isBoolean('this is string')).to.equal(false);
        expect(TypeHelper.isBoolean([])).to.equal(false);
        expect(TypeHelper.isBoolean(1)).to.equal(false);
        expect(TypeHelper.isBoolean({})).to.equal(false);
        expect(TypeHelper.isBoolean(true)).to.equal(true);
        expect(TypeHelper.isBoolean(false)).to.equal(true);
    });

    it('check null type', () => {
        expect(TypeHelper.isNull('null')).to.equal(false);
        expect(TypeHelper.isNull(null)).to.equal(true);
    });

    it('check undefined type', () => {
        expect(TypeHelper.isUndefined('undefined')).to.equal(false);
        expect(TypeHelper.isUndefined(undefined)).to.equal(true);
    });

    it('check empty', () => {
        expect(TypeHelper.isEmpty('this is string')).to.equal(false);
        expect(TypeHelper.isEmpty([])).to.equal(true);
        expect(TypeHelper.isEmpty(1)).to.equal(false);
        expect(TypeHelper.isEmpty({})).to.equal(true);
        expect(TypeHelper.isEmpty({ a: 1})).to.equal(false);
        expect(TypeHelper.isEmpty([1, 2, 3])).to.equal(false);
        expect(TypeHelper.isEmpty(null)).to.equal(true);

        let test; // undefined
        expect(TypeHelper.isEmpty(test)).to.equal(true);
    });
});
