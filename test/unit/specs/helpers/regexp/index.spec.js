import RegexpHelper from '@/helpers/regexp';

describe('@/helpers/regexp/index', () => {
    it('email => true', () => {
        expect(RegexpHelper.isEmail('94296417@qq.com')).to.equal(true);
    });
    it('email => false', () => {
        expect(RegexpHelper.isEmail('94296417qq.com')).to.equal(false);
    });
    it('url => true', () => {
        expect(RegexpHelper.isUrl('http://www.baidu.com')).to.equal(true);
    });
    it('url => false', () => {
        expect(RegexpHelper.isUrl('94296417@qq.com')).to.equal(false);
    });
    it('color => true', () => {
        expect(RegexpHelper.isColor('#ffffff')).to.equal(true);
    });
    it('color => false', () => {
        expect(RegexpHelper.isColor('#fffffff')).to.equal(false);
    });
});
