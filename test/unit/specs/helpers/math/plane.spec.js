import Plane from '@/helpers/math/plane';

describe('@/helpers/math/plane', () => {
    it('getPointQuadrant() = 0 / 1 / 2 / 3 / 4 / null', () => {
        expect(Plane.getPointQuadrant({ x: 0, y: 0 })).to.equal(0);
        expect(Plane.getPointQuadrant({ x: 1, y: 1 })).to.equal(1);
        expect(Plane.getPointQuadrant({ x: -1, y: 1 })).to.equal(2);
        expect(Plane.getPointQuadrant({ x: -1, y: -1 })).to.equal(3);
        expect(Plane.getPointQuadrant({ x: 1, y: -1 })).to.equal(4);
        expect(Plane.getPointQuadrant({ x: 0, y: 1 })).to.equal(null);
    });
    it('getTwoPointsDistance() = 5', () => {
        let t1 = Plane.getTwoPointsDistance({ x: 3, y: 4 });
        expect(t1).to.equal(5);
    });
    it('getTwoPointsAngle() = 45 / -135 / 135', () => {
        let t1 = Plane.getTwoPointsAngle({ x: 5, y: 5 }, { x: 10, y: 10 });
        expect(t1).to.equal(45);
        let t2 = Plane.getTwoPointsAngle({ x: 100, y: 100 });
        expect(t2).to.equal(-135);
    });
    it('getTwoVectorsAngle() = -45 / 45', () => {
        let a1 = Plane.getTwoVectorsAngle({ x: 30, y: 0 }, { x: 20, y: 20 });
        expect(a1.toFixed(2)).to.equal((-45).toFixed(2));
        let a2 = Plane.getTwoVectorsAngle({ x: 20, y: 20 }, { x: 30, y: 0 });
        expect(a2.toFixed(2)).to.equal((45).toFixed(2));
    });
    it('getTwoVectorsScale() = 0.5', () => {
        let s1 = Plane.getTwoVectorsScale({ x: -150, y: -200 }, { x: 400, y: 300 });
        expect(s1.toFixed(2)).to.equal((0.5).toFixed(2));
    });
});
